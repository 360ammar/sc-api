const development = {
  database: 'shiaclou_api',
  username: 'shiaclou_user',
  password: '.-Rz~%EQ}Doh',
  host: 'localhost',
  dialect: 'postgres',
  ssl: true,
   operatorAliases: false,
  "dialectOptions":{
      "ssl":{
         "require":true
      }
   }
};
const testing = {
  database: 'databasename',
  username: 'username',
  password: 'password',
  host: 'localhost',
  dialect: 'postgres',
   ssl: true,
   operatorAliases: false,
  "dialectOptions":{
      "ssl":{
         "require":true
      }
   }
};

const production = {
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST || 'localhost',
  dialect: 'postgres',
   ssl: true,
   operatorAliases: false,
  "dialectOptions":{
      "ssl":{
         "require":true
      }
   }
};

module.exports = {
  development,
  testing,
  production,
};
