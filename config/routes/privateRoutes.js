const privateRoutes = {
  // User Routes
  'GET /users': 'UserController.getAll',

  // Publisher Routes
  'POST /publisher/artist/add': 'ArtistController.addArtist',
  'POST /publisher/artist/all': 'ArtistController.getAllPubliserArtist',
  'POST /publisher/album/findproject': 'AlbumController.findProject',
  'POST /publisher/product/create' : 'AlbumController.createProduct',
  'POST /publisher/product/get' : 'AlbumController.getProduct',
  'POST /publisher/product/basic' : 'AlbumController.basicProduct',
  'POST /publisher/artist/get' : 'ArtistController.getArtist',

  'POST /genre/create' : 'OptionsController.createGenre',
  'POST /subgenre/create' : 'OptionsController.createSubGenre',
  
  'POST /genre/update' : 'OptionsController.updateGenre',
  'POST /subgenre/update' : 'OptionsController.updateSubGenre',
  'GET /genre/get' : 'OptionsController.getGenre',
  'GET /subgenre/get' : 'OptionsController.getSubGenre',

  'POST /publisher/product/marketing/add' : 'AlbumController.addMarketingInfo',
  'POST /publisher/product/marketing/get' : 'AlbumController.getMarketingInfo',

  
  'POST /publisher/track/add': 'TrackController.addTrack',
  'POST /publisher/track/get' : 'TrackController.getCurrentTrack',
  'POST /publisher/track/index' : 'TrackController.updateIndex',
  'POST /publisher/artist/delete' : 'ArtistController.deleteArtist'

};

module.exports = privateRoutes;
