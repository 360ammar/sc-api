const publicRoutes = {
  'POST /user': 'UserController.register',
  'POST /register': 'UserController.register', // alias for POST /user
  'POST /login': 'UserController.login',
  'POST /validate': 'UserController.validate',
  'POST /publisher/register': 'PublisherController.register', // alias for POST /user
  'POST /publisher/login': 'PublisherController.login',
  //Album Routes
  'GET /album/all': 'AlbumController.getAll',
  'POST /album/add': 'AlbumController.addAlbum',

  'POST /album/search': 'AlbumController.findAlbum',
  'POST /album/update': 'AlbumController.updateAlbum',
  'POST /album/addproject': 'AlbumController.addProject',
  'POST /album/editproject': 'AlbumController.editProject',
  'POST /album/findMyProject' : 'AlbumController.getAllProjects',

  'GET /artist/all': 'ArtistController.getAll',
  

  'POST /artist/get': 'ArtistController.findArtist',
  'POST /artist/update': 'ArtistController.updateArtist',
  'POST /option/add': 'OptionsController.setValue',
  'POST /option/update': 'OptionsController.updateOption',    
  'POST  /option/get': 'OptionsController.getOption',
  'POST  /track/get': 'TrackController.getTrack',
  'GET /track/all': 'TrackController.getAll',
  'GET /t/15chart' : 'ChartController.getChart',
  'POST /playlist/add' : 'PlaylistController.createPlaylist',
  'POST /playlist/track/add' : 'PlaylistController.addtoPlaylist',
  'POST /playlist/track/delete' : 'PlaylistController.deletefromPlaylist',
  'POST /playlist/delete' : 'PlaylistController.deletePlaylist',
  'POST /playlist/get' : 'PlaylistController.getPlaylist',
  'POST /playlist/getUserPlaylist' : 'PlaylistController.getUserPlaylist',
  'POST /artist/search': 'ArtistController.searchArtist',
  'POST /processtrack' : 'TrackController.processTrack',
  'POST /featuredslider' : 'FeaturedController.updateSlider',
  'POST /featuredtrack' : 'FeaturedController.updateTrackList',
  'POST /featuredartist' : 'FeaturedController.updateArtistList',
  'POST /featuredalbum' : 'FeaturedController.updateAlbumList',
  'POST /getFeatured' : 'FeaturedController.getFeaturedContent', 

  'POST /publisher/product/all': 'PublisherController.getAllPubliserAlbum',
  'POST /publisher/project/all': 'PublisherController.getAllPubliserProject',
  
};

module.exports = publicRoutes;
