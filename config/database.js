const Sequelize = require('sequelize');
const path = require('path');

const connection = require('./connection');

const db = {};

switch (process.env.NODE_ENV) {
  case 'production':
    sequelize = new Sequelize(
      connection.production.database,
      connection.production.username,
      connection.production.password, {
        host: connection.production.host,
        dialect: connection.production.dialect,
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      },
    );
    break;
  case 'testing':
    sequelize = new Sequelize(
      connection.testing.database,
      connection.testing.username,
      connection.testing.password, {
        host: connection.testing.host,
        dialect: connection.testing.dialect,
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      },
    );
    break;
  default:
    sequelize = new Sequelize(
      connection.development.database,
      connection.development.username,
      connection.development.password, {
        host: connection.development.host,
        dialect: connection.development.dialect,
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      },
    );
}
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('../api/models/User.js')(sequelize, Sequelize);
db.album = require('../api/models/Album.js')(sequelize, Sequelize);
db.artist = require('../api/models/Artist.js')(sequelize, Sequelize);
db.track = require('../api/models/Track.js')(sequelize, Sequelize);
db.options = require('../api/models/Options.js')(sequelize, Sequelize);
db.artist_album = require('../api/models/ArtistAlbum.js')(sequelize, Sequelize);
db.album_track = require('../api/models/AlbumTrack.js')(sequelize, Sequelize);
db.artist_track = require('../api/models/ArtistTrack.js')(sequelize, Sequelize);
db.primaryartist = require('../api/models/PrimaryArtists.js')(sequelize, Sequelize);
db.featuredartist = require('../api/models/FeaturedArtists.js')(sequelize, Sequelize);
db.remixers = require('../api/models/Remixers.js')(sequelize, Sequelize);
db.producers = require('../api/models/Producers.js')(sequelize, Sequelize);
db.writers = require('../api/models/Writers.js')(sequelize, Sequelize);
db.likes = require('../api/models/TrackLikes.js')(sequelize, Sequelize);
db.top15chart = require('../api/models/Top15Chart.js')(sequelize, Sequelize);
db.playlist_track = require('../api/models/PlaylistTracks.js')(sequelize, Sequelize);
db.playlist = require('../api/models/Playlist.js')(sequelize, Sequelize);
db.projectartist = require('../api/models/ProjectArtists.js')(sequelize, Sequelize);
db.projectalbum = require('../api/models/ProjectAlbum.js')(sequelize, Sequelize);
db.project = require('../api/models/Projects.js')(sequelize, Sequelize);
db.publisher = require('../api/models/Publisher.js')(sequelize, Sequelize);
db.publisherartist = require('../api/models/PublisherArtist.js')(sequelize, Sequelize);
db.publisheralbum = require('../api/models/PublisherAlbum.js')(sequelize, Sequelize);
db.publishertrack = require('../api/models/PublisherTrack.js')(sequelize, Sequelize);
db.publisherproject = require('../api/models/PublisherProject.js')(sequelize, Sequelize);
db.featured = require('../api/models/Featured.js')(sequelize, Sequelize);
db.genre = require('../api/models/Genre.js')(sequelize, Sequelize);
db.subgenre = require('../api/models/SubGenre.js')(sequelize, Sequelize);




//Primary Artist Associations
db.primaryartist.belongsTo(db.track);
db.primaryartist.belongsTo(db.artist);
db.primaryartist.belongsTo(db.album);
db.track.hasMany(db.primaryartist);
db.artist.hasMany(db.primaryartist);
db.album.hasMany(db.primaryartist);

//Featured Artist Associations
db.featuredartist.belongsTo(db.track);
db.featuredartist.belongsTo(db.artist);
db.featuredartist.belongsTo(db.album);
db.track.hasMany(db.featuredartist);
db.artist.hasMany(db.featuredartist);
db.album.hasMany(db.featuredartist);

//Remixers Associations
db.remixers.belongsTo(db.track);
db.remixers.belongsTo(db.artist);
db.remixers.belongsTo(db.album);
db.track.hasMany(db.remixers);
db.artist.hasMany(db.remixers);
db.album.hasMany(db.remixers);

//Producers Associations
db.producers.belongsTo(db.track);
db.producers.belongsTo(db.artist);
db.producers.belongsTo(db.album);
db.track.hasMany(db.producers);
db.artist.hasMany(db.producers);
db.album.hasMany(db.producers);

//Writers Associations
db.writers.belongsTo(db.track);
db.writers.belongsTo(db.artist);
db.track.hasMany(db.writers);
db.artist.hasMany(db.writers);

//Likes Disklikes Association

db.likes.belongsTo(db.track);
db.likes.belongsTo(db.user);
db.user.hasMany(db.likes);

// Playlist Track Association
db.playlist_track.belongsTo(db.playlist);
db.playlist_track.belongsTo(db.track);
db.playlist.hasMany(db.playlist_track);
db.track.hasMany(db.playlist_track);

//Playlist user association

db.playlist.belongsTo(db.user);
db.user.hasMany(db.playlist);


// Artist and Album Association
db.artist_album.belongsTo(db.artist);
db.artist_album.belongsTo(db.album);
db.album.hasMany(db.artist_album);
db.artist.hasMany(db.artist_album);


db.projectartist.belongsTo(db.artist);
db.projectartist.belongsTo(db.project);
db.project.hasMany(db.projectartist);
db.artist.hasMany(db.projectartist);

db.publisheralbum.belongsTo(db.publisher);
db.publisherartist.belongsTo(db.publisher);
db.publishertrack.belongsTo(db.publisher);
db.publisheralbum.belongsTo(db.album);
db.publisherartist.belongsTo(db.artist);
db.publishertrack.belongsTo(db.track);
db.album.hasMany(db.publisheralbum);
db.artist.hasMany(db.publisherartist);
db.track.hasMany(db.publishertrack);
db.publisher.hasMany(db.publisheralbum);
db.publisher.hasMany(db.publishertrack);
db.publisher.hasMany(db.publisherartist);
db.publisher.hasMany(db.publisherproject);
db.project.hasMany(db.publisherproject);
db.publisherproject.belongsTo(db.publisher);
db.publisherproject.belongsTo(db.project);

db.projectalbum.belongsTo(db.project);
db.projectalbum.belongsTo(db.album);
db.album.hasMany(db.projectalbum);
db.project.hasMany(db.projectalbum);

db.genre.hasMany(db.subgenre);
db.subgenre.belongsTo(db.genre);

db.top15chart.belongsTo(db.track);

db.featured.belongsTo(db.track);
db.featured.belongsTo(db.artist);
db.featured.belongsTo(db.album);

db.track.hasMany(db.featured);
db.artist.hasMany(db.featured);
db.album.hasMany(db.featured);


// Artist and Tracks Association
db.artist_track.belongsTo(db.artist);
db.artist_track.belongsTo(db.track);
db.track.hasMany(db.artist_track);
db.artist.hasMany(db.artist_track);

// Album and Track Association
db.album_track.belongsTo(db.album);
db.album_track.belongsTo(db.track);
db.track.hasMany(db.album_track);
db.album.hasMany(db.album_track);


module.exports = db;
