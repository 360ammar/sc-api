const bcryptService = require('../services/bcrypt.service');
const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};
module.exports = (sequelize, DataTypes) => {
const tableName = 'publishers';

const Publisher = sequelize.define('Publishers', {
  publisher_name: {
    type: DataTypes.STRING,
  },
  email : {
    type: DataTypes.STRING,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
  },
  image: {
    type: DataTypes.STRING,
  },
 

}, { hooks, tableName });

// eslint-disable-next-line
Publisher.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  delete values.password;

  return values;
};

return Publisher;
};
