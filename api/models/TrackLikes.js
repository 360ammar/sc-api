module.exports = (sequelize, DataTypes) => {
const tableName = 'track_likes';

const TrackLikes = sequelize.define('likes', {
	TrackId: {
    type: DataTypes.INTEGER,
  },
  UserId: {
    type: DataTypes.INTEGER,
  },
  vote: {
  	type: DataTypes.INTEGER,
  },

}, { tableName });
return TrackLikes;
};
