const sequelizePaginate = require('sequelize-paginate');
module.exports = (sequelize, DataTypes) => {
const tableName = 'publisher_project';
const PublisherProject = sequelize.define('publisher_project', {}, { tableName });
sequelizePaginate.paginate(PublisherProject);
return PublisherProject;
};
