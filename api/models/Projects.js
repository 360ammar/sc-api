const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, DataTypes) => {
const tableName = 'projects';
const Projects = sequelize.define('project', {
	 	project_name: {
    		type: DataTypes.STRING,
  		},
  		code: {
    		type: DataTypes.STRING,
  		},
  		description: {
    		type: DataTypes.STRING,
  		},
}, { tableName });
sequelizePaginate.paginate(Projects);
return Projects;
};
