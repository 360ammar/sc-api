module.exports = (sequelize, DataTypes) => {
    const tableName = 'Genre';
    
    const Genre = sequelize.define('Genre', {
      genre: {
        type: DataTypes.STRING,
      }
    
    }, { tableName });
    
    
    
    return Genre;
    };
    