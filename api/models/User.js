const bcryptService = require('../services/bcrypt.service');
const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};
module.exports = (sequelize, DataTypes) => {
const tableName = 'users';

const User = sequelize.define('User', {
  first_name: {
    type: DataTypes.STRING,
  },
  last_name: {
    type: DataTypes.STRING,
  },
  email: {
    type: DataTypes.STRING,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
  },
  birthday : {
    type : DataTypes.DATE,
  },
  gender : {
    type : DataTypes.STRING,
  },
  city: {
    type: DataTypes.STRING,
  },
  country: {
    type: DataTypes.STRING,
  },
  role: {
    type: DataTypes.ENUM('free','pro','publisher','admin'),
    defaultValue : 'free',
  },
  isVerified: {
    type: DataTypes.BOOLEAN,
    defaultValue : false,
  },
}, { hooks, tableName });

// eslint-disable-next-line
User.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  delete values.password;

  return values;
};

return User;
};
