const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, DataTypes) => {
const tableName = 'playlist';

const Playlist = sequelize.define('playlist', {

	title : {
		type : DataTypes.STRING,
	},

}, { tableName });
sequelizePaginate.paginate(Playlist);

return Playlist;
};
