const sequelizePaginate = require('sequelize-paginate');
module.exports = (sequelize, DataTypes) => {
const tableName = 'publisher_album';
const PublisherAlbum = sequelize.define('publisher_album', {}, { tableName });
sequelizePaginate.paginate(PublisherAlbum);
return PublisherAlbum;
};
