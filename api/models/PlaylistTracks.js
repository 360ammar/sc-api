module.exports = (sequelize, DataTypes) => {
const tableName = 'playlist_track';

const PlaylistTracks = sequelize.define('playlisttrack', {}, { tableName });
return PlaylistTracks;
};
