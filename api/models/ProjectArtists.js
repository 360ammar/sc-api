module.exports = (sequelize, DataTypes) => {
const tableName = 'project_artists';

const ProjectArtists = sequelize.define('projectartist', {}, { tableName });
return ProjectArtists;
};
