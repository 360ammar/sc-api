const sequelizePaginate = require('sequelize-paginate');
module.exports = (sequelize, DataTypes) => {
const tableName = 'publisher_artist';
const PublisherArtist = sequelize.define('publisher_artist', {}, { tableName });


sequelizePaginate.paginate(PublisherArtist);
return PublisherArtist;
};
