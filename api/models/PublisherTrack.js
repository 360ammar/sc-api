const sequelizePaginate = require('sequelize-paginate');
module.exports = (sequelize, DataTypes) => {
const tableName = 'publisher_track';
const PublisherTrack = sequelize.define('publisher_track', {}, { tableName });
sequelizePaginate.paginate(PublisherTrack);
return PublisherTrack;
};
