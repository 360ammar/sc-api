const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, DataTypes) => {
const tableName = 'artist';

const Artist = sequelize.define('Artist', {
  name: {
    type: DataTypes.STRING,
  },
  biography: {
    type: DataTypes.TEXT,
  },
  location: {
    type: DataTypes.STRING,
  },
  image: {
    type: DataTypes.STRING,
  },
  image220 : {
    type: DataTypes.STRING,
  },
  image150 : {
    type: DataTypes.STRING,
  },
  image140 : {
    type: DataTypes.STRING,
  },
  image110 : {
    type: DataTypes.STRING,
  },
  image70 :  {
    type: DataTypes.STRING,
  },
  image50 :  {
    type: DataTypes.STRING,
  },
  image38 : {
    type: DataTypes.STRING,
  },
  dob: {
    type: DataTypes.DATE,
  },
  year_from: {
    type: DataTypes.DATE,
  },
  year_to: {
    type: DataTypes.DATE,
  },
  active_now: {
    type: DataTypes.BOOLEAN,
  },
  followers: {
    type: DataTypes.INTEGER,
  },
  genre: {
    type: DataTypes.STRING,
  },
  sub_genre: {
    type: DataTypes.STRING,
  },

}, { tableName });

sequelizePaginate.paginate(Artist);

return Artist;
};
