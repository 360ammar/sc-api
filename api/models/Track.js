const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, DataTypes) => {
const tableName = 'track';

const Track = sequelize.define('Track', {
  track_name: {
    type: DataTypes.STRING,
  },
  uid: {
    type: DataTypes.STRING,
  },
  mediaFile: {
    type: DataTypes.STRING,
  },
  artwork: {
    type: DataTypes.STRING,
  },
  artwork220 : {
    type: DataTypes.STRING,
  },
  artwork150 : {
    type: DataTypes.STRING,
  },
  artwork140 : {
    type: DataTypes.STRING,
  },
  artwork110 : {
    type: DataTypes.STRING,
  },
  artwork70 :  {
    type: DataTypes.STRING,
  },
  artwork50 :  {
    type: DataTypes.STRING,
  },
  artwork38 : {
    type: DataTypes.STRING,
  },
  name_displayed: {
    type: DataTypes.STRING,
  },
  track_language: {
    type: DataTypes.STRING,
  },
  index: {
    type: DataTypes.INTEGER,
  },
  isrc: {
    type: DataTypes.STRING,
  },
  line: {
    type: DataTypes.STRING,
  },
  lyrics: {
    type: DataTypes.TEXT,
  },
  master_rights: {
    type: DataTypes.STRING,
  },
  recording_country: {
    type: DataTypes.STRING,
  },
  localized_language: {
    type: DataTypes.STRING,
  },
  duration: {
    type: DataTypes.STRING,
  },
  publishing_obligation: {
    type: DataTypes.STRING,
  },
  publishers: {
    type: DataTypes.STRING,
  },

}, { tableName });
sequelizePaginate.paginate(Track);

return Track;
};
