module.exports = (sequelize, DataTypes) => {
const tableName = 'featured_content';

const Featured = sequelize.define('Featured', {
  priority: {
    type: DataTypes.STRING,
  },
  type: {
    type: DataTypes.ENUM('featured_slider', 'featured_track' , 'featured_artist' , 'featured_radio','featured_album'),
  },
  TrackId: {
  	type: DataTypes.INTEGER,
  },
  ArtistId: {
  	type: DataTypes.INTEGER,
  },
  AlbumId: {
  	type: DataTypes.INTEGER,
  },
  RadioId: {
  	type: DataTypes.INTEGER,
  },
  featured_image : {
    type: DataTypes.STRING,
  },
  slide_name : {
    type : DataTypes.STRING
  }

}, { tableName });



return Featured;
};
