module.exports = (sequelize, DataTypes) => {
    const tableName = 'SubGenre';
    
    const SubGenre = sequelize.define('SubGenre', {
      sub_genre: {
        type: DataTypes.STRING,
      }
    }, { tableName });
    
    
    
    return SubGenre;
    };
    