const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, DataTypes) => {
  const tableName = 'album';
const Album = sequelize.define('Album', {
  release_date: {
    type: DataTypes.DATEONLY,
  },
  artwork: {
    type: DataTypes.STRING,
  },
  artwork220 : {
    type: DataTypes.STRING,
  },
  artwork150 : {
    type: DataTypes.STRING,
  },
  artwork140 : {
    type: DataTypes.STRING,
  },
  artwork110 : {
    type: DataTypes.STRING,
  },
  artwork70 :  {
    type: DataTypes.STRING,
  },
  artwork50 :  {
    type: DataTypes.STRING,
  },
  artwork38 : {
    type: DataTypes.STRING,
  },
  upc_code:{
    type: DataTypes.STRING,
  },
  meta_language: {
    type: DataTypes.STRING,
  },
  name: {
    type: DataTypes.STRING,
  },
  name_displayed: {
    type: DataTypes.STRING,
  },
  product_code: {
    type: DataTypes.STRING,
  },
  published: {
    type: DataTypes.INTEGER,
  },
  genre: {
    type: DataTypes.STRING,
  },
  sub_genre: {
    type: DataTypes.STRING,
  },
  format: {
    type: DataTypes.STRING,
  },
  imprint: {
    type: DataTypes.JSON,
  },
  marketing: {
    type: DataTypes.JSON,
  },
  copy_line: {
    type: DataTypes.STRING,
  },
  product_highlights: {
    type: DataTypes.TEXT,
  },
  special_instructions: {
    type: DataTypes.TEXT,
  },
  localized_language: {
    type: DataTypes.STRING,
  },

}, { tableName });
sequelizePaginate.paginate(Album);

return Album;
};
