module.exports = (sequelize, DataTypes) => {
const tableName = 'options';

const Options = sequelize.define('Options', {
  option_name: {
    type: DataTypes.STRING,
  },
  option_value: {
    type: DataTypes.STRING,
  },

}, { tableName });



return Options;
};
