const db = require('../../config/database');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const uniqid = require('uniqid');
const mm = require('music-metadata');
const TrackController = () => {



processAudio = (filepath) => {

   mm.parseFile('media/' + filepath , {native: true})
      .then(function (metadata) {
        console.log(metadata);
      })
      .catch(function (err) {
        console.error(err.message);
      });

}


const processTrack = async (req, res) => {
  const { body } = req;
  try {
        const track = await db.track.findOne({
          where : { id : body.trackid }
        }).then(response => processAudio(response.mediaFile));
      } catch (err) {
        console.log(err);
      }
}




  const addTrack = async (req, res) => {
    const { body } = req;
    const uid = uniqid(); 
      try {
        const track = await db.track.update({
          track_name: body.track_name,
          name_displayed: body.name_displayed,
          track_language: body.track_language,
          isrc: body.isrc,
          line: body.line,
          lyrics: body.lyrics,
          master_rights: body.master_rights,
          recording_country: body.recording_country,
          uid: uid,
          publishing_obligation: body.publishing_obligation,
          publishers: body.publishers
         },{ where: {  id: body.trackId }
        }).then(function(trackres){
          

          const { 
            primary_artists,
            featured_artists,
            remixers,
            producers,
             writers 
          } = body

          const primary_artist = Array.from(primary_artists)
          const featured_artist = Array.from(featured_artists)
          const remixer = Array.from(remixers) 
          const producer = Array.from(producers)
          const song_writers = Array.from(writers);



          function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
          }

          var allArtists = [];

          if (primary_artist.length > 0) {
            primary_artist.forEach(function (result) {
              db.primaryartist.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              });
              allArtists.push(result);
            });
          }

          if (featured_artist.length > 0) {
            featured_artist.forEach(function (result) {
              db.featuredartist.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              }
              );
              allArtists.push(result);
            });
          }

          if (remixer.length > 0) {
            remixer.forEach(function (result) {
              db.remixers.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              });
              allArtists.push(result);
            });
          }

          if (producer.length > 0) {
            producer.forEach(function (result) {
              db.producers.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              });
              allArtists.push(result);
            });


          }
          if (song_writers.length > 0) {

            song_writers.forEach(function (result) {
              db.writers.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              });

              allArtists.push(result);

            });

          }

          if (allArtists.length > 0) {
            var unique = allArtists.filter(onlyUnique);
            unique.forEach(function (result) {
              db.artist_track.findOrCreate(
              {
                where : {
                  TrackId: body.trackId,
                  ArtistId: result.value
                }
              });

            });
          }
          return res.status(200).json({ trackres });
        
        }).catch(function(err){
          console.log(err)
          return res.status(501).json({ err });
        });

      } catch (err) {
        console.log(err)
        return res.status(501).json({ err });
        }
  };


  const getTrack = async (req, res) => {
    const { body } = req;

    if (body.id || body.uid) {
      try {
        const track = await db.track.findOne({
            where: {
                  $or: [{id: body.id}, {uid: body.uid}]
            },
            include: [{ model: db.album_track,
                      include : [ {model: db.album,
                          include: [{ model: db.artist_album,
                            include: [ db.artist ] 
                          }]
                       }]
             }]
          }).then(function(result){
             // db.album_track.findOne({where {"id": }})
              
              // obj = JSON.parse(result);
              var obj = result.albumtracks;
              var arr = new Array();
              var val = obj[0].Album.artist_albums;

               var jsonval = new Object();




              val.forEach(function(res){

                jsonval.name = res.Artist.name;
                jsonval.id = res.Artist.id;

              });
                             var jsonString= JSON.stringify(jsonval);

              console.log(jsonString);
              return res.status(200).json({ "track_name" : result.name_displayed, "file" : result.mediaFile, 
                "album_name" : obj[0].Album.name_displayed,
                "artwork" : obj[0].Album.artwork,
                   "artwork220" : obj[0].Album.artwork220,
          "artwork150" : obj[0].Album.artwork150,
          "artwork140" : obj[0].Album.artwork140,
          "artwork110" : obj[0].Album.artwork110,
          "artwork70" :  obj[0].Album.artwork70,
          "artwork50" :  obj[0].Album.artwork50,
          "artwork38" : obj[0].Album.artwork38,
                "duration" : result.duration,

          "artwork" :result.artwork,
          "artwork220" : result.artwork220,
          "artwork150" : result.artwork150,
          "artwork140" : result.artwork140,
          "artwork110" : result.artwork110,
          "artwork70" :  result.artwork70,
          "artwork50" :  result.artwork50,
          "artwork38" : result.artwork38,
                



              });
  
          });

        if (!track) {
          return res.status(400).json({ msg: 'Track not found' });
        }

          

      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }else{

    return res.status(400).json({ msg: 'Please provide a value.' });
  }};

const getAll = async (req, res) => {

      try {
        const track = await db.track.findAll(
          {
             include: [{ model: db.album_track,
                       include : [ {model: db.album}]},
                       { model: db.primaryartist,
                        include : [db.artist]
                        } ],


         limit : req.query.limit || 20,
        offset : req.query.offset || 0,
        order : sequelize.literal(req.query.order  || 'id ASC') 
          }
          ).then(function(result){
            var tracks = [];

            result.forEach(function(res){
             var obj_artists = res.primaryartists;
             var album = res.albumtracks;

              var artists = [];

              obj_artists.forEach(function(res){
                var obj = {
                  "artist_id" : res.Artist.id,
                  "artist_name" : res.Artist.name

                };
                artists.push(obj);
              });
              var obj = {
                "id" : res.id,
                "uid" : res.uid,
                "track_name" : res.track_name,
                "file" : res.mediaFile,
                "artwork" :res.artwork,
          "artwork220" : res.artwork220,
          "artwork150" : res.artwork150,
          "artwork140" : res.artwork140,
          "artwork110" : res.artwork110,
          "artwork70" :  res.artwork70,
          "artwork50" :  res.artwork50,
          "artwork38" : res.artwork38,
                 "album_id" : album[0].Album.id,
                 "album_name" : album[0].Album.name,
                 artists
              };

              tracks.push(obj);
            });



            // obj_artists.forEach(function(res){
            //   var obj = {
            //     id : res.Artist.id,
            //     artist_name : res.Artist.name 
            //   };
            //   artists.push(obj);
            // });


              return res.status(200).json({ 
                tracks

                // "id" : result.id,
                // "uid" : result.uid,
                // "track_name" : result.track_name,
                // "file" : result.mediaFile,
                // "artwork" :result.artwork,
                // //"album_name" : album[0].Album.name_displayed,
                // artists
                 });
  
          });

        if (!track) {
          return res.status(400).json({ msg: 'Track not found' });
        }

          

      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
      };

      const updateIndex = (req,res) => {
        
        try {
          const { body } = req;
          const { indexes } = body;
          indexes.map( async (value) => {

            await db.track.update({
                index : value.index
            },
            {  where : {
                id : value.id
              }
            }
            ).then(val => {
              console.log(val)
            }).catch(err => {
              console.log(err)
            })
          })
          
          return res.status(200).json({ track : 'success' })

          
        } catch (error) {
          return res.status(501).json({ error })   
          
        }

       }


       const getCurrentTrack = async (req,res) => {
        
        try {
          const { body } = req;
        const track = await db.track.findOne(
          {
            include: [{
                    model: db.primaryartist,
                    include: [db.artist]
                },
                {
                    model: db.featuredartist,
                    include: [db.artist]
                },
                {
                    model: db.producers,
                    include: [db.artist]
                },
                {
                    model: db.remixers,
                    include: [db.artist]
                },
                {
                    model: db.writers,
                    include: [db.artist]
                },
              ],
              where: { id : body.trackId }
          }).then(track => {
              return res.status(200).json({ track })
          }).catch(err => {
            return res.status(501).json({ err })   
         
          })

          
        } catch (error) {
          return res.status(501).json({ error })   
          
        }

       }

  return {
    addTrack,
    getTrack,
    getAll,
    processTrack,
    getCurrentTrack,
    updateIndex
  };
};

module.exports = TrackController;
