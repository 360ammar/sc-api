const db = require('../../config/database');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const Op = db.Sequelize.Op;

const PublisherController = () => {
  const register = async (req, res) => {
    const { body } = req;

    if (body.password === body.password2) {
      try {
        const publisher = await db.publisher.create({
          publisher_name: body.publisher_name,
          email: body.email,
          password: body.password,
          image: ''
        });
        const token = authService().issue({ id: publisher.id });

        return res.status(200).json({ token, publisher });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: 'Bad Request: Passwords don\'t match' });
  };

  const login = async (req, res) => {
    const { email, password } = req.body;

    if (email && password) {
      try {
        const publisher = await db.publisher
          .findOne({
            where: {
              email,
            },
          });

        if (!publisher) {
          return res.status(400).json({ msg: 'Bad Request: publisher not found' });
        }

        if (bcryptService().comparePassword(password, publisher.password)) {
          const token = authService().issue({ id: publisher.id });

          return res.status(200).json({ token, publisher });
        }else{   return res.status(401).json({ msg: 'Unauthorized' }); }
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

  };

  const validate = (req, res) => {
    const { token } = req.body;

    authService().verify(token, (err) => {
      if (err) {
        return res.status(401).json({ isvalid: false, err: 'Invalid Token!' });
      }
      return res.status(200).json({ isvalid: true });
    });
  };

  const getAll = async (req, res) => {
    try {
      const publisher = await db.publisher.findAll();

      return res.status(200).json({ publisher });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAllPubliserAlbum = async (req, res) => {
    try
    {
      const { body } = req
      const albumPublisher = await db.publisheralbum.paginate(
        
      {  include:[ { model: db.album ,
       
        include : [ { model : db.projectalbum} , { model : db.artist_album ,
            include : [ db.artist ]
        }], 
         
      }
      ],
       page: body.page || 1,
      paginate: body.paginate || 15,
    },
   
      { where : { PublisherId : req.body.publisherID,
                  'Album.name'  :  {
                    [Op.like]: '%' + req.body.name + '%'
                  }
      }  },
      {attributes :  ['AlbumId',  ['Album','id'] ]  },
      );
      

      return res.status(200).json({albumPublisher});
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAllPubliserProject = async (req, res) => {
    try
    
    {
      const { body } = req
      const projectPublisher = await db.publisherproject.paginate(
        
      {  include:[ { model: db.project }
      //   ,{ where : { 'project.name'  :  {
      //   [Op.like]: '%' + req.body.name + '%'
      // } }}

    ],
          where : { PublisherId : req.body.publisherID,
            // 'project.name'  :  {
            //   [Op.like]: '%' + req.body.name + '%'
            // }
      },
      
      page: body.page || 1,
      paginate: body.paginate || 15,

      });

      return res.status(200).json({projectPublisher});
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  return {
    register,
    login,
    validate,
    getAll,
    getAllPubliserAlbum,
    getAllPubliserProject
  };
};

module.exports = PublisherController;
