const db = require('../../config/database');
const authService = require('../services/auth.service');

const OptionsController = () => {
  const setValue = async (req, res) => {
    const { body } = req;
      try {
        const option = await db.options.create({
          option_name: body.option_name,
          option_value: body.option_value,
        });
        return res.status(200).json({ option });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
  };


    const createGenre = async (req, res) => {
      const { body } = req;
        try {
          const genre = await db.genre.create({
            genre: body.genre
          });
          return res.status(200).json({ genre });
        } catch (err) {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getGenre = async (req, res) => {
        try {
          const genre = await db.genre.findAll();
          return res.status(200).json({ genre });
        } catch (err) {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const getSubGenre = async (req, res) => {
      try {
        const subgenre = await db.subgenre.findAll(
         { include : [{ model: db.genre }] }
        );
        return res.status(200).json({ subgenre });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    const updateGenre = async (req, res) => {
      const { body } = req;
        try {
          const genre = await db.genre.update({
            genre: body.genre
          },
          { where: {id : body.id} }
          );
          return res.status(200).json({ genre });
        } catch (err) {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        }
    }


    const createSubGenre = async (req, res) => {
      const { body } = req;
        try {
          const genre = await db.subgenre.create({
            sub_genre: body.sub_genre,
            GenreId: body.GenreId
          });
          return res.status(200).json({ genre });
        } catch (err) {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        }
    }

    const updateSubGenre = async (req, res) => {
      const { body } = req;
        try {
          const genre = await db.subgenre.update({
            sub_genre: body.sub_genre,

          },
            {where : {GenreId: body.GenreId}}
          );
          return res.status(200).json({ genre });
        } catch (err) {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        }
    }
  

  const getOption = async (req, res) => {
    const { option_name } = req.body;

    if (option_name) {
      try {
        const option = await db.options.findOne({
            where: {
              option_name,
            },
          });

        if (!option) {
          return res.status(400).json({ msg: 'Option not found' });
        }

          return res.status(200).json({ option });


      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: 'Please provide a value.' });
  };

   const updateOption = async (req, res) => {
    const { option_name, option_value } = req.body;




        try {
            const option = await db.options.update(
                { option_value: option_value },
                { where: { option_name: option_name } }
              )
                .then(function(result){
                    return res.status(200).json({ result });
              })

                .catch(err =>  handleError(err))




          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }



  };


  return {
    setValue,
    getOption,
    updateOption,
    createGenre,
    createSubGenre,
    updateGenre,
    updateSubGenre,
    getGenre,
    getSubGenre
  };
};

module.exports = OptionsController;
