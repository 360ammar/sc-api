const db = require('../../config/database');
const authService = require('../services/auth.service');
const Op = db.Sequelize.Op;
const ArtistController = () => {
  const addArtist = async (req, res) => {
    const { body } = req;
    console.log(body);
    console.log(body.name);
    console.log(body.publisherId)
      try {
        const artist = await db.artist.create({
          name: body.name,
          biography: body.bio,
          dob: body.dob, 
          location: body.location,
          year_from: body.year_from,
          year_to: body.year_to,
          active_now: body.active_now,
        }).then(respone => 
          {
            db.publisherartist.create({
              ArtistId : respone.id , PublisherId: req.body.publisherId,
            }); 
            return res.status(200).json({ respone });
        });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
  };

  const getArtist = async (req, res) => {
    const {id} = req.body;
    try {
      const artist = await db.artist.findOne({
          where: {id}}).then(artist =>  res.status(200).json({ artist }));
        
      }
      catch(err) {res.status(500).json({err})};
    }; 

  const findArtist = async (req, res) => {
    const { id } = req.body;
    console.log(req);
    if (id) {
      try {
        const artist = await db.artist.findOne({
            where: {
              id,
            },
            		include:[ { model: db.artist_track,
                  include : [ {model : db.track, 
                      include : [ {model : db.album_track,
                          include : [db.album]
                       } ]
                  } ]
                }, { model : db.artist_album,
                  include : [ db.album ]
                }

                 ]
          
          }).then(artists => {
            var albums = [];
            var getAlbums = artists.artist_albums;

            var tracks = [];
            var getTracks = artists.artisttracks;
            if(getAlbums){
            getAlbums.forEach(function(result){
              if(result.Album != null){
              var obj = {
                id : result.Album.id, 
                artwork : result.Album.artwork,
                  artwork220 : result.Album.artwork220,
                  artwork150 : result.Album.artwork150,
                  artwork140 : result.Album.artwork140,
                  artwork110 : result.Album.artwork110,
                  artwork70 : result.Album.artwork70,
                  artwork50 : result.Album.artwork50,
                  artwork38 : result.Album.artwork38,

                album_name : result.Album.name_displayed
              };
              albums.push(obj);
            }

            }); 
          }
            getTracks.forEach(function(result){
              if(result.Track != null){
              var obj = {
                id : result.Track.uid, 
                track_name : result.Track.name_displayed,
                file : result.Track.mediaFile,
                duration : result.Track.duration,
                album_name : result.Track.albumtracks[0].Album.name_displayed,
                album_id : result.Track.albumtracks[0].Album.id,
                artist_name : artists.name

              };
              tracks.push(obj);
            }
            }); 




      res.status(200).json({
          "id" : artists.id,
          "name" : artists.name,
          "biography" :artists.biography,
          "year_from" : artists.year_from,
          "year_to" : artists.year_to,
          "active_now" : artists.active_now,
          "dob" : artists.dob,
          "image": artists.image,
          "image220" : artists.image220,
          "image150" : artists.image150,
          "image140" : artists.image140,
          "image110" : artists.image110,
          "image70" :  artists.image70,
          "image50" :  artists.image50,
          "image38" : artists.image38,
          "genre" : artists.genre,
          "sub_genre" : artists.sub_genre,
                 albums , tracks
            

       } )
    });




      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

  };

  const updateArtist = async (req, res) => {
    const { name, id } = req.body;
        try {
            const artist = await db.artist.update(
                { name: name },
                { where: { id: id } }
              )
                .then(function(result){
                    return res.status(200).json({ result });
              })

                .catch(err =>  handleError(err))

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }



  };


   const searchArtist = async (req, res) => {
     const {name} = req.body;
     console.log(name);
    try {
      const artist = await db.artist.findAll({
        limit : req.query.limit || 10,
         where: {

          name: {
        [Op.like]: '%' + name + '%'
      }
        }
      }     ).then(function(result){
        return res.status(200).json({ result });

      });

      
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAll = async (req, res) => {
    try {
      const artist = await db.artist.paginate();

      return res.status(200).json({ artist });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAllPubliserArtist = async (req, res) => {
    try
    {
      const artistPublisher = await db.publisherartist.paginate(
        
      {  include:[ { model: db.artist } ] },
   
      { where : { PublisherId : req.body.publisherID,
                  'Artist.name'  :  {
                    [Op.like]: '%' + req.body.name + '%'
                  }
      }  },
      {attributes :  ['ArtistId',  ['Artist','id'] ]  },
      { page : req.body.page || 1 }
      );
      

      return res.status(200).json({artistPublisher});
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  

  const deleteArtist = async (req, res) => {
    const { id } = req.body;
        try {
            const artist = await db.publisherartist.destroy(
                { where: { ArtistId: id } }
              )
                .then(function(result){
                 
                  db.primaryartist.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.featuredartist.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.remixers.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.producers.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.writers.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.artist_album.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.projectartist.destroy(
                    { where: { ArtistId: id } }
                  );
                
                  db.featured.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.artist_track.destroy(
                    { where: { ArtistId: id } }
                  );
                  db.artist.destroy(
                    { where: { id: id } }
                  );
                  
                    return res.status(200).json({ result });
              })

                .catch(err =>  console.log(err))

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }



  };

  return {
    addArtist,
    findArtist,
    updateArtist,
    getAll,
    searchArtist,
    getAllPubliserArtist,
    getArtist,
    deleteArtist
  };
};

module.exports = ArtistController;
