const db = require('../../config/database');
const authService = require('../services/auth.service');

const FeaturedController = () => {

     const updateSlider = async (req, res) => {
    const { priority, ArtistId, TrackId, AlbumId, RadioId } = req.body;
       try {
            const featured = await db.featured.find({
             where: {
              $or: [
              {ArtistId: ArtistId},
               {TrackId: TrackId},
               {AlbumId : AlbumId},
               {RadioId : RadioId}
               ],
                type: 'featured_slider'
              }
            }).then(function(obj){
              console.log(obj);
                if(obj){
                  obj.update({
                      priority : priority,
                      TrackId : TrackId,
                      AlbumId : AlbumId,
                      ArtistId : ArtistId,
                      RadioId : RadioId
                  });
                }
                else {
                  db.featured.create({
                    priority : priority,
                    ArtistId : ArtistId,
                    AlbumId : AlbumId,
                    TrackId : TrackId,
                    type : 'featured_slider',
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };


   const updateTrackList = async (req, res) => {
    const { priority, TrackId } = req.body;
       try {
            const featured = await db.featured.find(
                { where: { TrackId: TrackId, type : 'featured_track' } }
              ).then(function(obj){
                  if(obj){
                    obj.update({
                      priority : priority,
                      trackID : TrackId,
                  });
                }
                else {
                  db.featured.create({
                    priority : priority,
                    TrackId : TrackId,
                    type : 'featured_track',
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };

   const updateArtistList = async (req, res) => {
    const { priority, ArtistId } = req.body;
       try {
            const featured = await db.featured.find(
                { where: { ArtistId: ArtistId , type : 'featured_artist' } }
              ).then(function(obj){
                  if(obj){
                    obj.update({
                      priority : priority,
                      ArtistId : ArtistId,
                  });
                }
                else {
                  db.featured.create({
                    priority : priority,
                    ArtistId : ArtistId,
                    type : 'featured_artist',
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };
     const updateAlbumList = async (req, res) => {
    const { priority, AlbumId } = req.body;
       try {
            const featured = await db.featured.find(
                { where: { AlbumId: AlbumId , type : 'featured_album' } }
              ).then(function(obj){
                  if(obj){
                    obj.update({
                      priority : priority,
                      AlbumId : AlbumId,
                  });
                }
                else {
                  db.featured.create({
                    priority : priority,
                    AlbumId : AlbumId,
                    type : 'featured_album',
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };


     const updateRadioList = async (req, res) => {
    const { priority, RadioId } = req.body;
       try {
            const featured = await db.featured.find(
                { where: { RadioId: RadioId , type : 'featured_radio' } }
              ).then(function(obj){
                  if(obj){
                    obj.update({
                      priority : priority,
                      RadioId : RadioId,
                  });
                }
                else {
                  db.featured.create({
                    priority : priority,
                    RadioId : RadioId,
                    type : 'featured_radio',
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };


 const getFeaturedContent = async (req, res) => {
       const { body } = req;
       
       try {
             const featured_content = []; 
            const featured = await db.featured.findAll({
              include : [ {model : db.artist}, {model : db.track}, {model : db.album} ],

              where : { type: body.type },

                            limit : req.query.limit || 50,
                            offset : req.query.offset || 0,
                            order : sequelize.literal(req.query.order || 'priority ASC') 

            })
                .then(function(result){
                  result.forEach(function(resp){
            if(body.type=='featured_slider'){
           
             var type = '';
             var id = 0;
             var name = '';



            if(resp.TrackId != null){
                id = resp.Track.id;
                name = resp.Track.track_name;
                type = 'Track';
            }
            if(resp.ArtistId != null){
               
                id = resp.Artist.id;
                name = resp.Artist.name;
                type = 'Artist';
            }
            if(resp.AlbumId != null){
                id = resp.Album.id;
                name = resp.Album.name;
                type = 'Album';
            }

              var obj = {
                featured_image : resp.featured_image,
                priority : resp.priority,
                id,
                name,
                type
              };
              featured_content.push(obj);
            }
          });

           result.forEach(function(resp){
            if(body.type=='featured_artist'){
            
             var type = '';
             var id = 0;
             var name = '';
             var artwork = '';
             var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 =''; 
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';

            if(resp.TrackId != null){
                id = resp.Track.id;
                name = resp.Track.track_name;
                type = 'Track';
                artwork = resp.Track.artwork;
                  artwork220 = resp.Track.artwork220;
          artwork150 = resp.Track.artwork150;
          artwork140 = resp.Track.artwork140;
          artwork110 = resp.Track.artwork110;
          artwork70 = resp.Track.artwork70;
          artwork50 = resp.Track.artwork50;
          artwork38 = resp.Track.artwork38;
            }
            if(resp.ArtistId != null){
               
                id = resp.Artist.id;
                name = resp.Artist.name;
                type = 'Artist';
                artwork = resp.Artist.image;
                 artwork150 =resp.Artist.image150;
          artwork140 =resp.Artist.image140;
          artwork110 =resp.Artist.image110;
          artwork70 = resp.Artist.image70;
          artwork50 = resp.Artist.image50;
          artwork38 = resp.Artist.image38;
            }
            if(resp.AlbumId != null){
                id = resp.Album.id;
                name = resp.Album.name;
                type = 'Album';
                artwork = resp.Album.artwork;
                     artwork220 = resp.Album.artwork220;
          artwork150 = resp.Album.artwork150;
          artwork140 = resp.Album.artwork140;
          artwork110 = resp.Album.artwork110;
          artwork70 = resp.Album.artwork70;
          artwork50 = resp.Album.artwork50;
          artwork38 = resp.Album.artwork38;
            }

              var obj = {
                custom_image : resp.featured_image,
                priority : resp.priority,
                id,
                name,
                type,
                artwork,
                   artwork220,
                artwork150,
          artwork140,
           artwork110,
           artwork70,
           artwork38,
           artwork50
              };
              featured_content.push(obj);
            }
          });

            result.forEach(function(resp){
            if(body.type=='featured_track'){
             
             var type = '';
             var id = 0;
             var name = '';
             var artwork = '';
             var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 =''; 
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';


            if(resp.TrackId != null){
                id = resp.Track.id;
                name = resp.Track.track_name;
                type = 'Track';
                artwork = resp.Track.artwork;
                  artwork220 = resp.Track.artwork220;
          artwork150 = resp.Track.artwork150;
          artwork140 = resp.Track.artwork140;
          artwork110 = resp.Track.artwork110;
          artwork70 = resp.Track.artwork70;
          artwork50 = resp.Track.artwork50;
          artwork38 = resp.Track.artwork38;
            }
            if(resp.ArtistId != null){
               
                id = resp.Artist.id;
                name = resp.Artist.name;
                type = 'Artist';
                artwork = resp.Artist.image;
                   artwork220 = resp.Artist.image220;
          artwork150 =resp.Artist.image150;
          artwork140 =resp.Artist.image140;
          artwork110 =resp.Artist.image110;
          artwork70 = resp.Artist.image70;
          artwork50 = resp.Artist.image50;
          artwork38 = resp.Artist.image38;
            }
            if(resp.AlbumId != null){
                id = resp.Album.id;
                name = resp.Album.name;
                type = 'Album';
                artwork = resp.Album.artwork;
                   artwork220 = resp.Album.artwork220;
          artwork150 = resp.Album.artwork150;
          artwork140 = resp.Album.artwork140;
          artwork110 = resp.Album.artwork110;
          artwork70 = resp.Album.artwork70;
          artwork50 = resp.Album.artwork50;
          artwork38 = resp.Album.artwork38;

            }

              var obj = {
                custom_image : resp.featured_image,
                priority : resp.priority,
                id,
                name,
                type,
                artwork,
                   artwork220,
                artwork150,
          artwork140,
           artwork110,
           artwork70,
           artwork38,
           artwork50
              };
              featured_content.push(obj);
            }
          });

             result.forEach(function(resp){
            if(body.type=='featured_album'){
             
             
             var type = '';
             var id = 0;
             var name = '';
             var artwork = '';
          var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 =''; 
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';


            if(resp.TrackId != null){
                id = resp.Track.id;
                name = resp.Track.track_name;
                type = 'Track';
                artwork = resp.Track.artwork;
                            artwork220 = resp.Track.artwork220;
          artwork150 = resp.Track.artwork150;
          artwork140 = resp.Track.artwork140;
          artwork110 = resp.Track.artwork110;
          artwork70 = resp.Track.artwork70;
          artwork50 = resp.Track.artwork50;
          artwork38 = resp.Track.artwork38;

            }
            if(resp.ArtistId != null){
               
                id = resp.Artist.id;
                name = resp.Artist.name;
                type = 'Artist';
                artwork = resp.Artist.image;
                  artwork220 = resp.Artist.image220;
          artwork150 =resp.Artist.image150;
          artwork140 =resp.Artist.image140;
          artwork110 =resp.Artist.image110;
          artwork70 = resp.Artist.image70;
          artwork50 = resp.Artist.image50;
          artwork38 = resp.Artist.image38;
            }
            if(resp.AlbumId != null){
                id = resp.Album.id;
                name = resp.Album.name;
                type = 'Album';
                artwork = resp.Album.artwork;
                    artwork220 = resp.Album.artwork220;
          artwork150 = resp.Album.artwork150;
          artwork140 = resp.Album.artwork140;
          artwork110 = resp.Album.artwork110;
          artwork70 = resp.Album.artwork70;
          artwork50 = resp.Album.artwork50;
          artwork38 = resp.Album.artwork38;
            }

              var obj = {
                custom_image : resp.featured_image,
                priority : resp.priority,
                id,
                name,
                type,
                artwork,
                artwork220,
                artwork150,
          artwork140,
           artwork110,
           artwork70,
           artwork38,
           artwork50
              };
              featured_content.push(obj);
            }
          });
        

                  return res.status(200).json({ featured_content });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };



  return {
    updateSlider,getFeaturedContent,
    updateTrackList,
    updateArtistList,
    updateAlbumList

  };
};

module.exports = FeaturedController;
