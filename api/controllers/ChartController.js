const db = require('../../config/database');
const authService = require('../services/auth.service');

const ChartController = () => {


   const updateChart = async (req, res) => {
    const { priority, trackID } = req.params;
       try {
            const chart = await db.top15chart.find(
                { where: { trackID: trackID } }
              )
                .then(function(obj){
                  if(obj){
                  db.top15chart.update({
                    priority : priority,
                    trackID : trackID,
                    where : { trackID : trackID } 
                  });
                }
                else {
                  db.top15chart.create({
                    priority : priority,
                    trackID : trackID,
                  });
                }
                    
              }).then(function(result){
                return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };

 const getChart = async (req, res) => {
       try {
            const chart = await db.top15chart.findAll({
              limit : 15,

            })
                .then(function(result){
                  return res.status(200).json({ result });
              });

          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }
  };



  return {
    updateChart,
    getChart
  };
};

module.exports = ChartController;
