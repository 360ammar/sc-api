const db = require('../../config/database');
const authService = require('../services/auth.service');

const PlaylistController = () => {
  const createPlaylist = async (req, res) => {
    const { body } = req;
      try {
        const playlist = await db.playlist.create({
          title : body.title,
          UserId : body.UserId, 
        });
        return res.status(200).json({ playlist });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
  };

  const getUserPlaylist = async (req , res) => {
    const {userid} = req.body;


      try {
        const userplaylist = await db.playlist.findAll({
            where: {
              UserId : userid,
            },
            include : [ { model : db.playlist_track,
                include : [{ model : db.track,
                  include: [{
                      model: db.album_track,
                      include: [db.album]
                  },
                  {model: db.artist_track,
                  include : [db.artist]
                }
                  ]
                 }]

        }]
          }).then(function(result){
             
              let playlistdata = [];

              result.forEach(function(resp){
                const playlistTracks = resp.playlisttracks;
                let tracksdata = [];
                  playlistTracks.forEach(function(response){
                    if(response.Track){
                    const tracks = response.Track;
                    const artist = tracks.artisttracks;
                    const album = tracks.albumtracks[0];
                    const albumarr = [];
                    if(album){
                      let obj = {
                        id: album.Album.id,
                        name: album.Album.name_displayed
                      };
                      albumarr.push(obj);
                    }  
                    let artists = [];
                    if(artist){
                      artist.forEach(function(resp){
                          let obj = {
                              artist_id : resp.Artist.id,
                              artist_name : resp.Artist.name
                          };
                          artists.push(obj);
                      });
                    }


                      let myobj ={
                         id: tracks.id ,
                    name : tracks.name_displayed,
                    type : 'track',
                    artwork : tracks.artwork,
                    artwork220 : tracks.artwork220,
                    artwork150 : tracks.artwork150,
                    artwork140 : tracks.artwork140,
                    artwork110 : tracks.artwork110,
                    artwork70 : tracks.artwork70,
                    artwork50 : tracks.artwork50,
                    artwork38 : tracks.artwork38,
                    createdAt : tracks.createdAt,
                    artists,
                    album: albumarr

                      };

                      tracksdata.push(myobj);
                    }
                  });

                let obj = 
              {
                  id: resp.id,
                  title: resp.title,
                  updatedAt: resp.updatedAt,
                  createdAt: resp.createdAt,
                  tracks : tracksdata
              };

                playlistdata.push(obj);
              });
            
            

             return res.status(200).json({ 
              playlists : playlistdata

              });
          });

        if (!userplaylist) {
          return res.status(400).json({ msg: 'Playlist not found' });
        }

         

      } catch (err) {
        console.log(err);
        const messag = 'Internal server error' + err.message;
        return res.status(500).json({ msg: messag  });
      }
    



  }

  const getPlaylist = async (req, res) => {
    const { id } = req.body;

    
      try {
        const playlist = await db.playlist.findOne({
            where: {
              id,
            },
            include : [ { model : db.playlist_track,
                include : [ { model : db.track,
                  
                          }]
               , include: [ { model : db.album_track,
                            include : [db.album]
                           }]

                 }] 
       
          });

        if (!playlist) {
          return res.status(400).json({ msg: 'Playlist not found' });
        }

          return res.status(200).json({ playlist });


      } catch (err) {
        console.log(err);
        const messag = 'Internal server error' + err.message;
        return res.status(500).json({ msg: messag  });
      }
    

  };

   const updatePlaylist = async (req, res) => {
    const { id, title } = req.body;
        try {
            const playlist = await db.playlist.update(
                { title : title },
                { where: { id } }
              )
                .then(function(result){
                    return res.status(200).json({ result });
              })
                .catch(err =>  handleError(err))
          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }

  };

  const addtoPlaylist = async(req, res ) => {
    const { id , trackID } = req.body;

    try {
      const playlist = await db.playlist_track.create({
          TrackId : trackID, playlistId : id
      }).then(function(result){
        return res.status(200).json({ result });
      });
    }
      catch (err) {
        console.log(err);
        return res.status(500).json({msg: 'An error occured'});
      }
  };
  const deletePlaylist = async(req, res ) => {
    const { id } = req.body;

    try {
      const playlist = await db.playlist.destroy({
          where : {id  : id}
      }).then(function(result){
        return res.status(200).json({ result });
      });
    }
      catch (err) {
        console.log(err);
        return res.status(500).json({msg: 'An error occured'});
      }
  };

  const deletefromPlaylist = async(req, res ) => {
    const { trackID } = req.body;

    try {
      const playlist = await db.playlist_track.destroy({
          where : {TrackId  : trackID}
      }).then(function(result){
        return res.status(200).json({ result });
      });
    }
      catch (err) {
        console.log(err);
        return res.status(500).json({msg: 'An error occured'});
      }
  };


  return {
    createPlaylist,
    getPlaylist,
    updatePlaylist,
    deletePlaylist,
    deletefromPlaylist,
    addtoPlaylist,
    getUserPlaylist
  };
};

module.exports = PlaylistController;
