const db = require('../../config/database');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const sharp = require('sharp');




const AlbumController = () => {



  const addProject = async (req, res) => {
    const { body } = req;
      try {
        const project = await db.project.create({
          project_name: body.project_name,
          code: body.code,
          description: body.description,
        }).then(function(next){
            const albumid = next.id;
            db.publisherproject.create({
              projectId: albumid, PublisherId: body.publisherId,
            });


         db.projectartist.create({
                     projectId: albumid, ArtistId: body.project_artist,
                    }).then((artist) => {
                        
            return res.status(200).json({ next , projectId : next.id , artist });  
                   } );
              
                        
          });
      } catch (err) {
        console.log(err);
      }
  };  


    const findProject = async (req, res) => {
    const { id } = req.body;
      try {
        const project = await db.project.findOne(
         
        {
          where : { id }
        ,

          include : [ {
            model: db.projectartist,
            include : [db.artist]
          },
          {
            model : db.projectalbum,
            include : [db.album]
          },
          
        ],

    
      }
        
         ).then(function(result){
          
            return res.status(200).json({ result });
          });
          
      } catch (err) {
        console.log(err);
      }
  };  

    const editProject = async (req, res) => {
    const { body } = req;
        try {
          const project = await db.project.update(
          {  project_name: body.project_name,
          code: body.code,
          description: body.description
          },
              {where : { id : body.id }}
          ).then(function(result){
             return res.status(200).json({ result });
            });
         } catch (err) {
          console.log(err);
        }
    };  


    const getAllProjects = async (req, res) => {
        const {body} = req;

        try{
          const projects = await db.publisherproject.findAll({
            include: [{model: db.project,
                include :[ { model : db.projectalbum ,
                  include: [{model : db.album,
                      include: [{model : db.album_track,
                          include: [db.track]
                      }]
                   }]
                 }]
            }],
                limit : req.query.limit || 20,
        offset : req.query.offset || 0,
            where : {PublisherId : body.publisherid }
          }).then(function(result){
            return res.status(200).json({ result });

          });

        } catch (err){
          console.log(err);
          return res.status(200).json({ err });
        }


    };

const createProduct = async (req, res) => {
  const { body } = req;
  try {
    const album = await db.album.create({
    upc_code: body.upc_code,
    name: body.name,
    format: body.format,
    product_code : body.product_code,
    name_displayed: body.name
    }).then(function(album){
      db.publisheralbum.create({
        AlbumId: album.id, PublisherId: body.publisherId,
      });
      db.projectalbum.create({
        AlbumId: album.id, projectId: body.projectId,
      });
      return res.status(200).json({ album });
    });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ message: 'An error occured!' });
    }
}; 

const basicProduct = async (req, res) => {
  const { body } = req;
  const {  primary_artists,
    featured_artists,
    remixers,
    producers} = body;
  try {
    const album = await db.album.update({
    upc_code: body.upc_code,
    name: body.name,
    format: body.format,
    product_code : body.product_code,
    meta_language: body.meta_language,
    localized_language : body.localized_language,
    release_date: body.release_date,
    genre: body.genre,
    sub_genre : body.sub_genre,
    imprint : body.imprint,
    copy_line: body.copy_line,
    product_highlights : body.product_highlights,
    special_instructions: body.special_instructions,
  
    
    },
    {where : {id : body.id}}
    ).then(function(resp){
      function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
      }

      // usage example:
      var allArtists = [];

      var p_artist = primary_artists.filter(onlyUnique);
      var f_artist = featured_artists.filter(onlyUnique);
      var r_mixers = remixers.filter(onlyUnique);
      var p_producers = producers.filter(onlyUnique);

      p_artist.forEach(function (result) {

        db.primaryartist.create({
          AlbumId: body.id, ArtistId: result.value
        });
        allArtists.push(result.value);
      });


      f_artist.forEach(function (result) {
        db.featuredartist.create({
          AlbumId: body.id, ArtistId: result.value
        });
        allArtists.push(result.value);
      });

      r_mixers.forEach(function (result) {
        db.remixers.create({
          AlbumId: body.id, ArtistId: result.value
        });
        allArtists.push(result.value);
      });

      p_producers.forEach(function (result) {
        db.producers.create({
          AlbumId: body.id, ArtistId: result.value
        });
        allArtists.push(result.value);
      });


      var unique = allArtists.filter(onlyUnique);

      unique.forEach(function (result) {
        db.artist_album.create({
          AlbumId: body.id, ArtistId: result,
        });

      });


      return res.status(200).json({ album: resp });
    });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ message: 'An error occured!' });
    }
};
    

  const addAlbum = async (req, res) => {
    const { body } = req;
      const primary_artists = body.primary_artists;
      const featured_artists = body.featured_artists;
      const remixers = body.remixers;
      const producers = body.producers;

      console.log(primary_artists + ' primary_artists');
      console.log(featured_artists + 'featured_artists');
      console.log(remixers + 'remixers');
      console.log(producers + 'procers');
   
      try {
          const album = await db.album.create({
          upc_code: body.upc_code,
          meta_language: body.meta_language, 
          name: body.name,
          name_displayed: body.name_displayed,
          genre: body.genre, 
          sub_genre: body.sub_genre, 
          format: body.format,
          imprint: body.imprint,
          copy_line: body.copy_line, 
          product_highlights: body.product_highlights, 
          special_instructions: body.special_instructions,
          localized_language: body.localized_language,
           }
          ).then(function(resp){

            function onlyUnique(value, index, self) { 
              return self.indexOf(value) === index;
            }

          // usage example:
         var allArtists = [];

          var p_artist = primary_artists.filter( onlyUnique );
          var f_artist = featured_artists.filter( onlyUnique );
          var r_mixers = remixers.filter( onlyUnique );
          var p_producers = producers.filter( onlyUnique );

          p_artist.forEach(function(result){
            db.primaryartist.create({
              AlbumId: resp.id, ArtistId: result
            });
            allArtists.push(result);
          });


          f_artist.forEach(function(result){
            db.featuredartist.create({
              AlbumId: resp.id, ArtistId: result
            });
           allArtists.push(result);
          });

          r_mixers.forEach(function(result){
            db.remixers.create({
              AlbumId: resp.id, ArtistId: result
            });
           allArtists.push(result);
          });

          p_producers.forEach(function(result){
            db.producers.create({
              AlbumId: resp.id, ArtistId: result
            });
          allArtists.push(result);
          });

          db.projectalbum.create({
            projectId: body.projectid,  AlbumId: resp.id
          });

          db.publisheralbum.create({
            PublisherId: body.publisherid,  AlbumId: resp.id
          });


          var unique = allArtists.filter( onlyUnique );


          unique.forEach(function(result){
            db.artist_album.create({
               AlbumId: resp.id, ArtistId: result,
          });
          
          });



                  console.log(resp);
                  return res.status(200).json(200,{ resp });
                 });


                
      } catch (err) {
        console.log(err);

      }

  };

  const findAlbum = async (req, res) => {
    const { id } = req.body;

    if (id) {
      try {
        const album = await db.album.findOne({
            where: {
              id,
            },
             include: [ { model : db.primaryartist,
                include : [db.artist]
             },  { model : db.featuredartist,
                include : [db.artist]
             }, { model : db.remixers,
                include : [db.artist]
             }, { model : db.producers,
                include : [db.artist]
             }, 
             { model : db.artist_album ,
                include : [db.artist]
             }, { model : db.album_track,
                include : [{model : db.track,
                  include : [ {model:db.artist_track,
                      include : [db.artist]
                   }]
                }]
             }

             ]
          }).then(function(result){

             function onlyUnique(value, index, self) { 
              return self.indexOf(value) === index;
          }


               var p_artist_result = result.primaryartists;
               var f_artist_result = result.featuredartists;
               var r_mixers_result = result.remixer_artists;
               var p_producers_result = result.producer_artists;

              var p_artist = p_artist_result.filter( onlyUnique );
              var f_artist = f_artist_result.filter( onlyUnique );
              var r_mixers = r_mixers_result.filter( onlyUnique );
              var p_producers = p_producers_result.filter( onlyUnique );

               var primaryartist = [];
               var featuredartists = [];
               var remixers = [];
               var producers = [];
               var remixers = [];
               p_artist.forEach(function(resp){
                var obj = {
                  id : resp.Artist.id,
                  name: resp.Artist.name,
                  artist_avatar : resp.Artist.image 
                };
                 primaryartist.push(obj);
               });
               f_artist.forEach(function(resp){
                var obj = {
                  id : resp.Artist.id,
                  name: resp.Artist.name,
                  artist_avatar : resp.Artist.image 
                };
                 featuredartists.push(obj);
               });
               r_mixers.forEach(function(resp){
                var obj = {
                  id : resp.Artist.id,
                  name: resp.Artist.name,
                  artist_avatar : resp.Artist.image 
                };
                 remixers.push(obj);
               });
               p_producers.forEach(function(resp){
                var obj = {
                  id : resp.Artist.id,
                  name: resp.Artist.name,
                  artist_avatar : resp.Artist.image 
                };
                 producers.push(obj);
               });

               var getTracks = result.albumtracks;
               var tracks = [];
                getTracks.forEach(function(result){
                    if(result.Track != null){
                    var obj = {
                      id : result.Track.id, 
                      track_name : result.Track.name_displayed,
                      file : result.Track.mediaFile,
                      album_name : result.name_displayed,
                      album_id : result.id,
                      artist_name : result.Track.artisttracks[0].Artist.name,
                      artist_id : result.Track.artisttracks[0].Artist.id,
                      artwork : result.Track.artwork,
          artwork220 : result.Track.artwork220,
          artwork150 : result.Track.artwork150,
          artwork140 : result.Track.artwork140,
          artwork110 : result.Track.artwork110,
          artwork70 : result.Track.artwork70,
          artwork50 : result.Track.artwork50,
          artwork38 : result.Track.artwork38,
          duration : result.Track.duration
                    };
                    tracks.push(obj);
                  }
                  }); 

               return res.status(200).json({ 
                "id" : result.id,
                "album_name" : result.name_displayed,

          "artwork" :result.artwork,
          "artwork220" : result.artwork220,
          "artwork150" : result.artwork150,
          "artwork140" : result.artwork140,
          "artwork110" : result.artwork110,
          "artwork70" :  result.artwork70,
          "artwork50" :  result.artwork50,
          "artwork38" : result.artwork38,
                "copy_line" :result.copy_line,
                "genre" : result.genre,
                "sub_genre" : result.sub_genre,
                "release_date" : result.release_date,
                  "primary_artists" : primaryartist,
                   "featured_artists" : featuredartists,
                   "remixers" : remixers,
                   "producers" : producers,

                tracks

                 });

          });

        if (!album) {
          return res.status(400).json({ msg: 'Album not found' });
        }

         


      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
else{
    return res.status(400).json({ msg: 'Please provide a value.' });
  }
};

  const getProduct = async (req, res) => {
    const { id, publisherId } = req.body;
      try {
        const publisheralbum = await db.publisheralbum.findOne({
          where: {
            AlbumId :id,
            PublisherId : publisherId ,
          },
        });
        
     
        if (!publisheralbum) {
          return res.status(400).json({ msg: 'Album not found' });
        }

        const album = await db.album.findOne({
          where: {
            id,
          },
          include: [{
            model: db.primaryartist,
            include: [db.artist]
          }, {
            model: db.featuredartist,
            include: [db.artist]
          }, {
            model: db.remixers,
            include: [db.artist]
          }, {
            model: db.producers,
            include: [db.artist]
          },
        {
          model: db.publisheralbum,
        },
          {
            model: db.artist_album,
            include: [db.artist]
          }, {
            model: db.album_track,
            include: [{
              model: db.track,
              include: [{
                model: db.artist_track,
                include: [db.artist]
              }]
            }]
          }

          ]
        }).then(function (result) {

          function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
          }


          var p_artist_result = result.primaryartists;
          var f_artist_result = result.featuredartists;
          var r_mixers_result = result.remixer_artists;
          var p_producers_result = result.producer_artists;

          var p_artist = p_artist_result.filter(onlyUnique);
          var f_artist = f_artist_result.filter(onlyUnique);
          var r_mixers = r_mixers_result.filter(onlyUnique);
          var p_producers = p_producers_result.filter(onlyUnique);

          var primaryartist = [];
          var featuredartists = [];
          var remixers = [];
          var producers = [];
          var remixers = [];
          p_artist.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image
            };
            primaryartist.push(obj);
          });
          f_artist.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image
            };
            featuredartists.push(obj);
          });
          r_mixers.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image
            };
            remixers.push(obj);
          });
          p_producers.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image
            };
            producers.push(obj);
          });

          var getTracks = result.albumtracks;
          var tracks = [];
          getTracks.forEach(function (result) {
            if (result.Track != null) {
              var obj = {
                id: result.Track.id,
                track_name: result.Track.name_displayed,
                file: result.Track.mediaFile,
                album_name: result.name_displayed,
                album_id: result.id,
                artist_name: result.Track.artisttracks.length > 0 ? result.Track.artisttracks[0].Artist.name : '',
                artist_id: result.Track.artisttracks.length > 0 ? result.Track.artisttracks[0].Artist.id : 0,
                artwork: result.Track.artwork,
                artwork220: result.Track.artwork220,
                artwork150: result.Track.artwork150,
                artwork140: result.Track.artwork140,
                artwork110: result.Track.artwork110,
                artwork70: result.Track.artwork70,
                artwork50: result.Track.artwork50,
                artwork38: result.Track.artwork38,
                duration: result.Track.duration
              };
              tracks.push(obj);
              }
          });

          return res.status(200).json({
           result,
            "primary_artists": primaryartist,
            "featured_artists": featuredartists,
            "remixers": remixers,
            "producers": producers,
            tracks,
            publisheralbum

          });
        
        });

        




      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    };




  const updateAlbum = async (req, res) => {
    const { name, id } = req.body;




        try {
            const album = await db.album.update(
                { name: name },
                { where: { id: id } }
              )
                .then(function(result){
                    return res.status(200).json({ result });
              })

                .catch(err =>  handleError(err))




          } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          }



  };

  const uploadArtwork = async (req, res) => {
     
      try {
        const {body} = req;
        console.log(body)
        
             console.log(req.file);
             const filepath = req.file.path;
             console.log(filepath);
             const resize = size => sharp(filepath)
             .resize(size, size)
             .toFile(`${filepath.slice(0,-4)}-${size}.jpg`);
         Promise
           .all([220,150,140,110,70,50,38].map(resize))
           .then(() => {
             console.log('complete');
           }).catch(err => console.log(err));
        
          const album = db.album.update({

            artwork : filepath.slice(6),
          artwork220 : filepath.slice(6,-4)+'-220.jpg' ,
          artwork150 : filepath.slice(6,-4)+'-150.jpg' ,
          artwork140 : filepath.slice(6,-4)+'-140.jpg' ,
          artwork110 : filepath.slice(6,-4)+'-110.jpg',
          artwork70 : filepath.slice(6,-4)+'-70.jpg', 
          artwork50 : filepath.slice(6,-4)+'-50.jpg', 
          artwork38 : filepath.slice(6,-4)+'-38.jpg',


          },
           {where: { id: body.productid } })
        .then(function(resp) {

                   db.options.findOne({
                                     where: {
                                         option_name: 'artwork_dir'
                                     }
                                 })
                                 .then(result => {
                                     let track_dir = result.option_value;
                                     track_dir = track_dir.slice(6).split('/').join("");

                                     if (trackres.id % 1000 == 0) {
                                         track_dir += 1;
                                         var my_string = '' + 1;
                                         console.log(my_string);
                                         while (my_string.length < 9) {
                                             my_string = '0' + my_string;
                                         }
                                         const dir_path = 'artwork-' + my_string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "/");
                                         db.options.update({
                                             option_value: dir_path
                                         }, {
                                             where: {
                                                 option_name: 'artwork_dir'
                                             }
                                         })

                                     }
                                 });
                           
           
                    return res.status(200).send({ resp });
             
        })

                
      } catch (err) {
        console.log(err);

      }

   
  }


  const getAll = async (req, res) => {
    try {
      const album = await db.album.findAndCountAll({
    
         include: [{model: db.artist_album, include : [db.artist]}],
              distinct: true,
                     limit : req.query.limit || 20,
        offset : req.query.offset || 0,
        order : sequelize.literal(req.query.order || 'id DESC')


      }).then(function(result){
        const albums = [];
        const data = result.rows;

        data.forEach(response => {

              const artist = response.artist_albums;  
              var artists = [];
              if(artist){
                artist.forEach(function(resp){
                    var obj = {
                        artist_id : resp.Artist.id,
                        artist_name : resp.Artist.name
                    };
                    artists.push(obj);
                });
              }

              var obj = {
                id: response.id ,
                name : response.name_displayed, 
                artwork : response.artwork,
                artwork220 : response.artwork220,
                artwork150 : response.artwork150,
                artwork140 : response.artwork140,
                artwork110 : response.artwork110,
                artwork70 : response.artwork70,
                artwork50 : response.artwork50,
                artwork38 : response.artwork38,
                createdAt : response.createdAt,
                artists
              };

                albums.push(obj)});


     



      return res.status(200).json({ "count" : result.count , albums  });
       });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const addMarketingInfo = async (req, res) => {
      try {
           const marketing = await db.album.update(
          { 
            marketing : req.body.marketing
           }
          ,{
          where: { id : req.body.productId }
        }).then(response => {
          return res.status(200).send({ response });
        }).catch(err => {
          
      console.log(err)
          return res.status(501).send({ err });
        })
      } catch (error) {
      
      console.log(error)
        return res.status(501).send({ error });
      }
  }

  const getMarketingInfo = async (req, res) => {
    try {

      const marketing = await db.album.findOne(
        {
          where: { id : req.body.productId }
      }).then(response => {
        return res.status(200).send({ response });
      }).catch(err => {
        
      console.log(err)
        return res.status(501).send({ err });
      })
    } catch (error) {
      console.log(error)
      return res.status(501).send({ error });
    }
}


  return {
    addAlbum,
    findAlbum,
    updateAlbum,
    getAll,
    addProject,
    findProject,
    editProject,
    getAllProjects,
    createProduct,
    getProduct,
    basicProduct,
    uploadArtwork,
    addMarketingInfo,
    getMarketingInfo
  };
};

module.exports = AlbumController;
