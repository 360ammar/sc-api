/**
 * third party libraries
 */
const bodyParser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const http = require('http');
const mapRoutes = require('express-routes-mapper');
const cors = require('cors');
const ChartController = './controllers/ChartController';
const logger = require('morgan');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const uniqid = require('uniqid');
const fileUpload = require('express-fileupload');
const mm = require('music-metadata');
const Jimp = require('jimp');
const sharp = require('sharp');
const ffmpeg = require('fluent-ffmpeg');
const exec = require('child_process').exec;
/**
 * server configuration
 */
const config = require('../config/');
const dbService = require('./services/db.service');
const auth = require('./policies/auth.policy');
const db = require('../config/database');
const loadCollection = require('./utils');
// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;
const Promise = require('bluebird');
global.Promise = Promise;

function getDestination() {
  const option = db.options.findOne({
    where: { option_name: 'track_dir' }
  })
    .then(result => {
      console.log(result);
      return result;
    });
};

function mkDirByPathSync(targetDir, { isRelativeToScript = false } = {}) {
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  const baseDir = isRelativeToScript ? __dirname : '.';

  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(baseDir, parentDir, childDir);
    try {
      fs.mkdirSync(curDir);
      console.log(`Directory ${curDir} created!`);
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err;
      }

      console.log(`Directory ${curDir} already exists!`);
    }

    return curDir;
  }, initDir);
}

let fname = '';
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    fname = uniqid();
    db.options.findOne({
      where: { option_name: 'track_dir' }
    })
      .then(result => {
        mkDirByPathSync('media/' + result.option_value);
        cb(null, 'media/' + result.option_value);
      })


  },
  filename: function (req, file, cb) {
    console.warn(file)
    cb(null, fname + path.extname(file.originalname));
  }
})

var upload = multer({ storage: storage })


// ARTWORK STORAGE multer


var artwork_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    fname = uniqid();

    db.options.findOne({
      where: { option_name: 'artwork_dir' }
    })
      .then(result => {
        cb(mkDirByPathSync('media/' + result.option_value), 'media/' + result.option_value);
      })

  },
  filename: function (req, file, cb) {

    cb(null, fname + path.extname(file.originalname));

  }

});

var artwork = multer({ storage: artwork_storage });

// Artist Image Multer

// ARTWORK STORAGE multer


let artist_storage = multer.diskStorage({
  destination: function (req, file, cb) {


    db.options.findOne({
      where: { option_name: 'artist_dir' }
    })
      .then(result => {
        mkDirByPathSync('media/' + result.option_value);
        cb(null, 'media/' + result.option_value);
      })

  },
  filename: function (req, file, cb) {
    fname = uniqid();
    cb(null, fname + path.extname(file.originalname));
  }
})

let artist_image = multer({ storage: artist_storage })

var track_art_storage = multer.diskStorage({
  destination: function (req, file, cb) {
    fname = uniqid();

    db.options.findOne({
      where: { option_name: 'artwork_track_dir' }
    })
      .then(result => {
        mkDirByPathSync('media/' + result.option_value);
        cb(null, 'media/' + result.option_value);
      })


  },
  filename: function (req, file, cb) {
    cb(null, fname + path.extname(file.originalname));
  }
})

var track_art = multer({ storage: track_art_storage })




/**
 * express application
 */
const app = express();
const server = http.Server(app);
const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'api/controllers/');
const mappedAuthRoutes = mapRoutes(config.privateRoutes, 'api/controllers/');
const DB = dbService(environment, config.migrate).start();






// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());
app.use(express.static('media'))
// secure express app
app.use(helmet({
  dnsPrefetchControl: false,
  frameguard: false,
  ieNoOpen: false,
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// parsing the request bodys
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// app.use(bodyParser.json());
app.use(logger('dev'));
// app.use(fileUpload());
app.use(function (err, req, res, next) {
  console.error(err.stack)
  next(err)
})


// secure your private routes with jwt authentication middleware
app.all('/private/*', (req, res, next) => auth(req, res, next));

app.get('/albums', async (req, res, next) => {

  try {
    const album = await db.album.findAll();

    return res.status(200).json({ album });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ msg: 'Internal server error' });
  }
});


app.post('/newartist', artist_image.single('avatar'), async (req, res) => {

  const { body } = req;
  const filepath = req.file.path;
  const resize = size => sharp(filepath)
    .resize(size, size)
    .toFile(`${filepath.slice(0, -4)}-${size}.jpg`);
  Promise
    .all([220, 150, 140, 110, 70, 50, 38].map(resize))
    .then(() => {
      console.log('complete');
    }).catch(
      err => console.error(err)
    );
  try {

    await db.artist.update(

      {
        image: filepath.slice(6),

        image220: filepath.slice(6, -4) + '-220.jpg',
        image150: filepath.slice(6, -4) + '-150.jpg',
        image140: filepath.slice(6, -4) + '-140.jpg',
        image110: filepath.slice(6, -4) + '-110.jpg',
        image70: filepath.slice(6, -4) + '-70.jpg',
        image50: filepath.slice(6, -4) + '-50.jpg',
        image38: filepath.slice(6, -4) + '-38.jpg',
      },
      { where: { id: body.id } }
    ).then(function (result) {

      return res.status(200).json({ result });
    });

  } catch (err) {
    console.log(err);
    return res.status(500).json({ err });
  }
});


app.post('/addartwork', artwork.single('artwork-file') , async (req, res) => {



  let filepath = req.file.path
  const { body } = req;

  const resize = size => sharp(filepath)
  .resize(size, size)
  .toFile(`${filepath.slice(0,  '-' + path.extname(req.file.filename).length)}-${size}${path.extname(req.file.filename)}`);
    Promise
      .all([220, 150, 140, 110, 70, 50, 38].map(resize))
      .then(() => {
        console.log('complete');
      
    
      const album = db.album.update({

        artwork: filepath.slice(6),
        artwork220: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-220' + path.extname(req.file.filename),
        artwork150: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-150' + path.extname(req.file.filename),
        artwork140: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-140' + path.extname(req.file.filename),
        artwork110: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-110' + path.extname(req.file.filename),
        artwork70: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-70' + path.extname(req.file.filename),
        artwork50: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-50' + path.extname(req.file.filename),
        artwork38: filepath.slice(6, '-' +path.extname(req.file.filename).length) + '-38' + path.extname(req.file.filename),
      },
        { where: { id: body.productid } })
        .then(function (artwork_upload) {

    
          return res.status(200).send({ artwork_upload });

        }).catch(err => {
          console.log(err)
          return res.status(500).send({ err });
        })

  }).catch(err => {
    console.log(err)
    return res.status(500).send({ error_resize: err });
  })



  });



  processAudio = (filepath) => {

    return mm.parseFile('media/' + filepath, { native: true })
      .then(function (metadata) {
        console.log(metadata.format.duration);
        return metadata.format.duration;
      })
      .catch(function (err) {
        console.error(err.message);
      });

  }


  app.post('/uploadtrack' , upload.single('track-file'), async (req, res) => {

    const filepath = req.file.path
    const { body } = req

    await mm.parseFile(filepath, { native: true }).then(function (meta) {

      db.track.findOne({
                    where: { id: body.trackid }
                  }).then(function (obj) {
                    if(obj){
                      obj          .update({
                        mediaFile: filepath.slice(6),
                        duration: meta.format.duration
                      }).then(function (track_upload) {

                        return res.status(200).json({ track_upload });
                      }).catch(err => {
                        return res.status(500).json({ resp : err });
                      });
                    }else{
                    db.track.create({
                      mediaFile: filepath.slice(6),
                      duration: meta.format.duration
                    }).then(function (track_upload) {

                      db.publishertrack.create({
                        PublisherId: body.publisherId,
                        TrackId: track_upload.id,
                      });
                      
                      db.album_track.create({
                        AlbumId: body.productId,
                        TrackId: track_upload.id,
                      });

                      return res.status(200).json({ track_upload });
                    }).catch(err => {
                      return res.status(500).json({ resp : err });
                    });
                  }
                  });
    })
    .catch(function (err) {
      console.error(err.message);
      return res.status(500).json({ resp : err });
    });

  })




  app.post('/addtrack', track_art.single('artwork-file'), async (req, res) => {
    // app.post('/addtrack', async (req, res) => {


    const uid = uniqid();
    const { body } = req;
    const primary_artists = JSON.parse("[" + body.primary_artists + "]");
    const featured_artists = JSON.parse("[" + body.featured_artists + "]");
    const remixers = JSON.parse("[" + body.remixers + "]");
    const producers = JSON.parse("[" + body.producers + "]");
    const song_writers = JSON.parse("[" + body.song_writers + "]");

    console.log(primary_artists + ' primary_artists');
    console.log(featured_artists + 'featured_artists');
    console.log(remixers + 'remixers');
    console.log(producers + 'procers');
    console.log(song_writers + ' song_writers');
    const filepath = req.file.path;
    const resize = size => sharp(filepath)
      .resize(size, size)
      .toFile(`${filepath.slice(0, -4)}-${size}.jpg`);
    Promise
      .all([220, 150, 140, 110, 70, 50, 38].map(resize))
      .then(() => {
        console.log('complete');
      });

    try {
      if (body.trackid == 'null') {
        await db.track.create({
          track_name: body.track_name,
          name_displayed: body.name_displayed,
          track_language: body.track_language,
          isrc: body.isrc,
          line: body.line,
          lyrics: body.lyrics,
          master_rights: body.master_rights,
          recording_country: body.recording_country,
          localized_language: body.localized_language,
          artwork: filepath.slice(6),
          artwork220: filepath.slice(6, -4) + '-220.jpg',
          artwork150: filepath.slice(6, -4) + '-150.jpg',
          artwork140: filepath.slice(6, -4) + '-140.jpg',
          artwork110: filepath.slice(6, -4) + '-110.jpg',
          artwork70: filepath.slice(6, -4) + '-70.jpg',
          artwork50: filepath.slice(6, -4) + '-50.jpg',
          artwork38: filepath.slice(6, -4) + '-38.jpg',
          duration: body.duration,
          publishing_obligation: body.publishing_obligation,
          publishers: body.publishers,
          uid: uid
        }).then(function (trackres) {
          function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
          }

          // usage example:
          var allArtists = [];

          if (primary_artists.length > 0) {
            primary_artists.forEach(function (result) {
              db.primaryartist.create({
                TrackId: trackres.id,
                ArtistId: result
              });
              allArtists.push(result);
            });
          }

          if (featured_artists.length > 0) {
            featured_artists.forEach(function (result) {
              db.featuredartist.create({
                TrackId: trackres.id,
                ArtistId: result
              });
              allArtists.push(result);
            });
          }

          if (remixers.length > 0) {
            remixers.forEach(function (result) {
              db.remixers.create({
                TrackId: trackres.id,
                ArtistId: result
              });
              allArtists.push(result);
            });
          }

          if (producers.length > 0) {
            producers.forEach(function (result) {
              db.producers.create({
                TrackId: trackres.id,
                ArtistId: result
              });
              allArtists.push(result);
            });


          }
          if (song_writers.length > 0) {

            song_writers.forEach(function (result) {
              db.writers.create({
                TrackId: trackres.id,
                ArtistId: result
              });

              allArtists.push(result);

            });

          }






          if (allArtists.length > 0) {
            var unique = allArtists.filter(onlyUnique);

            console.log(unique);
            console.log(body.albumid);
            unique.forEach(function (result) {
              db.artist_track.create({
                TrackId: trackres.id,
                ArtistId: result
              });

            });
          }

          db.album_track.create({
            TrackId: trackres.id,
            AlbumId: body.albumid
          });



          db.options.findOne({
            where: {
              option_name: 'track_dir'
            }
          })
            .then(result => {
              let track_dir = result.option_value;
              tracK_dir = track_dir.slice(6).split('/').join("");

              if (trackres.id % 1000 == 0) {
                track_dir += 1;
                var my_string = '' + 1;
                console.log(my_string);
                while (my_string.length < 9) {
                  my_string = '0' + my_string;
                }
                const dir_path = 'track-' + my_string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "/");
                db.options.update({
                  option_value: dir_path
                }, {
                    where: {
                      option_name: 'track_dir'
                    }
                  })

              }
            });


          return res.status(200).json({ trackres, "id": trackres.id });
        });
      } else {
        await db.track.update({
          track_name: body.track_name,
          name_displayed: body.name_displayed,
          track_language: body.track_language,
          isrc: body.isrc,
          line: body.line,
          lyrics: body.lyrics,
          master_rights: body.master_rights,
          recording_country: body.recording_country,
          localized_language: body.localized_language,
          artwork: filepath.slice(6),
          artwork220: filepath.slice(6, -4) + '-220.jpg',
          artwork150: filepath.slice(6, -4) + '-150.jpg',
          artwork140: filepath.slice(6, -4) + '-140.jpg',
          artwork110: filepath.slice(6, -4) + '-110.jpg',
          artwork70: filepath.slice(6, -4) + '-70.jpg',
          artwork50: filepath.slice(6, -4) + '-50.jpg',
          artwork38: filepath.slice(6, -4) + '-38.jpg',
          duration: body.duration,
          publishing_obligation: body.publishing_obligation,
          publishers: body.publishers,
          uid: uid
        },
          {
            where: { id: body.trackid }
          }).then(function (trackres) {
            function onlyUnique(value, index, self) {
              return self.indexOf(value) === index;
            }

            // usage example:
            var allArtists = [];


            if (primary_artists.length > 0) {
              primary_artists.forEach(function (result) {
                db.primaryartist.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });
                allArtists.push(result);
              });
            }

            if (featured_artists.length > 0) {
              featured_artists.forEach(function (result) {
                db.featuredartist.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });
                allArtists.push(result);
              });
            }

            if (remixers.length > 0) {
              remixers.forEach(function (result) {
                db.remixers.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });
                allArtists.push(result);
              });
            }

            if (producers.length > 0) {
              producers.forEach(function (result) {
                db.producers.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });
                allArtists.push(result);
              });
            }
            if (song_writers.length > 0) {

              song_writers.forEach(function (result) {
                db.writers.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });

                allArtists.push(result);

              });

            }






            if (allArtists.length > 0) {
              var unique = allArtists.filter(onlyUnique);

              console.log(unique);
              console.log(body.albumid);
              unique.forEach(function (result) {
                db.artist_track.create({
                  TrackId: body.trackid,
                  ArtistId: result
                });

              });
            }

            db.album_track.create({
              TrackId: body.trackid,
              AlbumId: body.albumid
            });



            db.options.findOne({
              where: {
                option_name: 'track_dir'
              }
            })
              .then(result => {
                let track_dir = result.option_value;
                tracK_dir = track_dir.slice(6).split('/').join("");

                if (trackres.id % 1000 == 0) {
                  track_dir += 1;
                  var my_string = '' + 1;
                  console.log(my_string);
                  while (my_string.length < 9) {
                    my_string = '0' + my_string;
                  }
                  const dir_path = 'track-' + my_string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "/");
                  db.options.update({
                    option_value: dir_path
                  }, {
                      where: {
                        option_name: 'track_dir'
                      }
                    })

                }
              });

            return res.status(200).json({ trackres, "id": body.trackid });

          });

      }


    } catch (err) {
      console.log(err);
      res.sendStatus(400);
    }
  })


  app.get('/featuredcontent', async (req, res) => {

    try {
      const featured = await db.featured.findAll({
        limit: req.query.limit || 50,
        offset: req.query.offset || 0,
        order: sequelize.literal(req.query.order || 'priority ASC')
      })
        .then(function (result) {
          return res.status(200).json({ result });
        });

    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }

  });







  app.get('/t/tracklike/:trackID/:uid/:vote', async (req, res) => {
    const { uid, trackID, vote } = req.params;

    try {
      const tracklike = await db.likes.findOne({
        where: { TrackId: trackID, UserId: uid }
      }).then(function (obj) {
        if (obj) {
          obj.update({
            vote: vote

          },
            { where: { UserId: uid, TrackId: trackID } }
          ).then(function (result) {
            return res.status(200).json(result);
          });
        }
        else {
          db.likes.create({
            TrackId: trackID, UserId: uid, vote: vote
          }).then(function (result) {
            return res.status(200).json(result);
          })

        }

      });

    } catch (err) {
      console.log(err);
      return res.status(500).json({ err, msg: 'Internal server error' });
    }

  });

  app.get('/t/trackgetlike/:trackID/:uid', async (req, res) => {
    const { uid, trackID } = req.params;

    try {
      const tracklike = await db.likes.findAll({
        where: { UserId: uid, TrackId: trackID },

      }).then(function (result) {
        return res.status(200).json(result);
      });

    } catch (err) {
      console.log(err);
      return res.status(500).json({ err, msg: 'Internal server error' });
    }

  });


  app.get('/search', async (req, res) => {

    const searchQuery = req.query.search;
    console.log(searchQuery);

    const album_search = db.album.findAll({
      where: {
        $or: [
          { name: { $iLike: '%' + searchQuery + '%' } },
          { name_displayed: { $iLike: '%' + searchQuery + '%' } },
          { meta_language: { $iLike: '%' + searchQuery + '%' } }
        ]
      }

    });


    const track_search = db.track.findAll({
      include: [{ model: db.artist_track, include: [db.artist] }],
      where: {
        $or: [
          { track_name: { $iLike: '%' + searchQuery + '%' } },
          { name_displayed: { $iLike: '%' + searchQuery + '%' } },
          { track_language: { $iLike: '%' + searchQuery + '%' } },
          { lyrics: { $iLike: '%' + searchQuery + '%' } }
        ]
      }

    });


    const artist_search = db.artist.findAll({
      where: {
        $or: [
          { name: { $iLike: '%' + searchQuery + '%' } },
          { biography: { $iLike: '%' + searchQuery + '%' } }
        ]
      }

    });



    Promise
      .all([album_search, track_search, artist_search]).then(function (result) {
        const albums = result[0];
        const tracks = result[1];
        const artists = result[2];

        const searched_albums = [];
        const searched_tracks = [];
        const searched_artists = [];

        albums.forEach(response => {

          var obj = {
            id: response.id,
            name: response.name_displayed,
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,

          };

          searched_albums.push(obj)
        });

        tracks.forEach(response => {

          const artist = response.artisttracks;
          var artists = [];
          if (artist) {
            artist.forEach(function (resp) {
              if (resp.Artist) {
                var obj = {
                  artist_id: resp.Artist.id,
                  artist_name: resp.Artist.name
                };
                artists.push(obj);
              }
            });
          }

          var obj = {
            id: response.id,
            name: response.name_displayed,
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,
            artists
          };

          searched_tracks.push(obj)
        });


        artists.forEach(response => {

          var obj = {
            id: response.id,
            name: response.name,
            image: response.image,
            image220: response.image220,
            image150: response.image150,
            image140: response.image140,
            image110: response.image110,
            image70: response.image70,
            image50: response.image50,
            image38: response.image38,

          };

          searched_artists.push(obj)
        });

        return res.status(200).json({ "albums": searched_albums, "tracks": searched_tracks, "artists": searched_artists });


      });

  });

  app.get('/newreleases', async (req, res) => {

    const album_release = db.album.findAndCountAll({
      include: [{ model: db.artist_album, include: [db.artist] }],
      distinct: true,
      limit: req.query.limit || 10,
      offset: req.query.offset || 0,
      order: [
        ['createdAt', 'DESC']
      ]
    });


    const track_release = db.track.findAndCountAll({
      include: [{ model: db.artist_track, include: [db.artist] }],
      distinct: true,
      limit: req.query.limit || 10,
      offset: req.query.offset || 0,
      order: [
        ['createdAt', 'DESC']
      ]
    });


    Promise
      .all([album_release, track_release]).then(function (result) {

        var arr = result[0].rows;
        var arr1 = result[1].rows;
        var count = result[0].count + result[1].count;
        var arr3 = [];
        arr.forEach(response => {

          const artist = response.artist_albums;
          var artists = [];
          if (artist) {
            artist.forEach(function (resp) {
              var obj = {
                artist_id: resp.Artist.id,
                artist_name: resp.Artist.name
              };
              artists.push(obj);
            });
          }

          var obj = {
            id: response.id,
            name: response.name_displayed,
            type: 'album',
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,
            createdAt: response.createdAt,
            artists,

          };

          arr3.push(obj)
        });
        arr1.forEach(response => {

          const artist = response.artisttracks;
          var artists = [];
          if (artist) {
            artist.forEach(function (resp) {
              var obj = {
                artist_id: resp.Artist.id,
                artist_name: resp.Artist.name
              };
              artists.push(obj);
            });
          }

          var obj = {
            id: response.id,
            name: response.name_displayed,
            type: 'track',
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,
            createdAt: response.createdAt,
            artists
          };

          arr3.push(obj)
        });
        arr3.sort(function (a, b) {
          var keyA = new Date(a.createdAt),
            keyB = new Date(b.createdAt);
          // Compare the 2 dates
          if (keyA < keyB) return 1;
          if (keyA > keyB) return -1;
          return 0;
        });
        return res.status(200).json({ "data": arr3, count });


      });

  });


  app.get('/homecontent', async (req, res) => {



    const featured_slider = [];
    const featured_artist = [];
    const featured_track = [];
    const featured_album = [];
    const featured = await db.featured.findAll({

      include: [{ model: db.artist }, {
        model: db.track,
        include: [{
          model: db.artist_track,
          include: [db.artist]
        }]
      }, { model: db.album }],

      order: sequelize.literal('priority ASC'),

    }).then(function (result) {

      result.forEach(function (resp) {
        if (resp.type == 'featured_slider') {
          if (featured_slider.length > 9) {
            return;
          }
          var type = '';
          var id = 0;
          var name = '';



          if (resp.TrackId != null) {
            id = resp.Track.id;
            name = resp.Track.name_displayed;
            type = 'Track';
          }
          if (resp.ArtistId != null) {

            id = resp.Artist.id;
            name = resp.Artist.name;
            type = 'Artist';
          }
          if (resp.AlbumId != null) {
            id = resp.Album.id;
            name = resp.Album.name;
            type = 'Album';
          }

          var obj = {
            featured_image: resp.featured_image,
            priority: resp.priority,
            id,
            name,
            type
          };
          featured_slider.push(obj);
        }
      });

      result.forEach(function (resp) {
        if (resp.type == 'featured_artist') {
          if (featured_artist.length > 9) {
            return;
          }
          var type = '';
          var id = 0;
          var name = '';
          var artwork = '';
          var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 = '';
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';

          if (resp.TrackId != null) {
            id = resp.Track.id;
            name = resp.Track.name_displayed;
            type = 'Track';
            artwork = resp.Track.artwork;
            artwork220 = resp.Track.artwork220;
            artwork150 = resp.Track.artwork150;
            artwork140 = resp.Track.artwork140;
            artwork110 = resp.Track.artwork110;
            artwork70 = resp.Track.artwork70;
            artwork50 = resp.Track.artwork50;
            artwork38 = resp.Track.artwork38;
          }
          if (resp.ArtistId != null) {

            id = resp.Artist.id;
            name = resp.Artist.name;
            type = 'Artist';
            artwork = resp.Artist.image;
            artwork150 = resp.Artist.image150;
            artwork140 = resp.Artist.image140;
            artwork110 = resp.Artist.image110;
            artwork70 = resp.Artist.image70;
            artwork50 = resp.Artist.image50;
            artwork38 = resp.Artist.image38;
          }
          if (resp.AlbumId != null) {
            id = resp.Album.id;
            name = resp.Album.name;
            type = 'Album';
            artwork = resp.Album.artwork;
            artwork220 = resp.Album.artwork220;
            artwork150 = resp.Album.artwork150;
            artwork140 = resp.Album.artwork140;
            artwork110 = resp.Album.artwork110;
            artwork70 = resp.Album.artwork70;
            artwork50 = resp.Album.artwork50;
            artwork38 = resp.Album.artwork38;
          }

          var obj = {
            custom_image: resp.featured_image,
            priority: resp.priority,
            id,
            name,
            type,
            artwork,
            artwork220,
            artwork150,
            artwork140,
            artwork110,
            artwork70,
            artwork38,
            artwork50
          };
          featured_artist.push(obj);
        }
      });

      result.forEach(function (resp) {
        if (resp.type == 'featured_track') {
          if (featured_track.length > 9) {
            return;
          }

          var type = '';
          var id = 0;
          var name = '';
          var artwork = '';
          var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 = '';
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';
          var artists = [];

          if (resp.TrackId != null) {
            var artistrel = resp.Track.artisttracks;

            artistrel.forEach(function (resl) {
              var obj = {
                artist_id: resl.Artist.id,
                artist_name: resl.Artist.name
              };

              artists.push(obj);
            });
            id = resp.Track.id;
            name = resp.Track.name_displayed;
            type = 'Track';
            artwork = resp.Track.artwork;
            artwork220 = resp.Track.artwork220;
            artwork150 = resp.Track.artwork150;
            artwork140 = resp.Track.artwork140;
            artwork110 = resp.Track.artwork110;
            artwork70 = resp.Track.artwork70;
            artwork50 = resp.Track.artwork50;
            artwork38 = resp.Track.artwork38;
          }
          if (resp.ArtistId != null) {

            id = resp.Artist.id;
            name = resp.Artist.name;
            type = 'Artist';
            artwork = resp.Artist.image;
            artwork220 = resp.Artist.image220;
            artwork150 = resp.Artist.image150;
            artwork140 = resp.Artist.image140;
            artwork110 = resp.Artist.image110;
            artwork70 = resp.Artist.image70;
            artwork50 = resp.Artist.image50;
            artwork38 = resp.Artist.image38;
          }
          if (resp.AlbumId != null) {
            id = resp.Album.id;
            name = resp.Album.name;
            type = 'Album';
            artwork = resp.Album.artwork;
            artwork220 = resp.Album.artwork220;
            artwork150 = resp.Album.artwork150;
            artwork140 = resp.Album.artwork140;
            artwork110 = resp.Album.artwork110;
            artwork70 = resp.Album.artwork70;
            artwork50 = resp.Album.artwork50;
            artwork38 = resp.Album.artwork38;

          }

          var obj = {
            custom_image: resp.featured_image,
            priority: resp.priority,
            id,
            name,
            type,
            artwork,
            artwork220,
            artwork150,
            artwork140,
            artwork110,
            artwork70,
            artwork38,
            artwork50,
            artists
          };
          featured_track.push(obj);
        }
      });

      result.forEach(function (resp) {
        if (resp.type == 'featured_album') {
          if (featured_album.length > 9) {
            return;
          }

          var type = '';
          var id = 0;
          var name = '';
          var artwork = '';
          var artwork220 = '';
          var artwork150 = '';
          var artwork140 = '';
          var artwork110 = '';
          var artwork70 = '';
          var artwork38 = '';
          var artwork50 = '';


          if (resp.TrackId != null) {
            id = resp.Track.id;
            name = resp.Track.name_displayed;
            type = 'Track';
            artwork = resp.Track.artwork;
            artwork220 = resp.Track.artwork220;
            artwork150 = resp.Track.artwork150;
            artwork140 = resp.Track.artwork140;
            artwork110 = resp.Track.artwork110;
            artwork70 = resp.Track.artwork70;
            artwork50 = resp.Track.artwork50;
            artwork38 = resp.Track.artwork38;

          }
          if (resp.ArtistId != null) {

            id = resp.Artist.id;
            name = resp.Artist.name;
            type = 'Artist';
            artwork = resp.Artist.image;
            artwork220 = resp.Artist.image220;
            artwork150 = resp.Artist.image150;
            artwork140 = resp.Artist.image140;
            artwork110 = resp.Artist.image110;
            artwork70 = resp.Artist.image70;
            artwork50 = resp.Artist.image50;
            artwork38 = resp.Artist.image38;
          }
          if (resp.AlbumId != null) {
            id = resp.Album.id;
            name = resp.Album.name;
            type = 'Album';
            artwork = resp.Album.artwork;
            artwork220 = resp.Album.artwork220;
            artwork150 = resp.Album.artwork150;
            artwork140 = resp.Album.artwork140;
            artwork110 = resp.Album.artwork110;
            artwork70 = resp.Album.artwork70;
            artwork50 = resp.Album.artwork50;
            artwork38 = resp.Album.artwork38;
          }

          var obj = {
            custom_image: resp.featured_image,
            priority: resp.priority,
            id,
            name,
            type,
            artwork,
            artwork220,
            artwork150,
            artwork140,
            artwork110,
            artwork70,
            artwork38,
            artwork50
          };
          featured_album.push(obj);
        }
      });





    });

    var mychart = [];
    var new_releases = [];

    const album_release = db.album.findAll({
      include: [{ model: db.artist_album, include: [db.artist] }],
      limit: 6,
      order: [
        ['createdAt', 'DESC']
      ]
    });


    const track_release = db.track.findAll({
      include: [{ model: db.artist_track, include: [db.artist] }],

      limit: 6,
      order: [
        ['createdAt', 'DESC']
      ]
    });

    Promise
      .all([album_release, track_release]).then(function (result) {
        result[0].forEach(function (response) {
          const artist = response.artist_albums;
          var artists = [];
          if (artist) {
            artist.forEach(function (resp) {
              var obj = {
                artist_id: resp.Artist.id,
                artist_name: resp.Artist.name
              };
              artists.push(obj);
            });
          }

          var obj = {
            id: response.id,
            name: response.name_displayed,
            type: 'album',
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,
            artists
          };

          new_releases.push(obj);
        });

        result[1].forEach(function (response) {

          const artist = response.artisttracks;
          var artists = [];
          if (artist) {
            artist.forEach(function (resp) {
              var obj = {
                artist_id: resp.Artist.id,
                artist_name: resp.Artist.name
              };
              artists.push(obj);
            });
          }

          var obj = {
            id: response.id,
            name: response.name_displayed,
            type: 'track',
            artwork: response.artwork,
            artwork220: response.artwork220,
            artwork150: response.artwork150,
            artwork140: response.artwork140,
            artwork110: response.artwork110,
            artwork70: response.artwork70,
            artwork50: response.artwork50,
            artwork38: response.artwork38,
            artists
          };

          new_releases.push(obj);
        });
      });


    db.top15chart.findAll({
      include: [{ model: db.track, include: [{ model: db.artist_track, include: [db.artist] }] }],
      limit: 15,
      order: sequelize.literal('priority ASC'),
    }).then(function (resp) {
      resp.forEach(function (result) {
        var artistrelation = result.Track.artisttracks;
        var artists = []
        artistrelation.forEach(function (res) {
          var obj = {
            artist_name: res.Artist.name,
            artist_id: res.Artist.id
          };
          artists.push(obj);
        });
        var obj = {
          id: result.TrackId,
          priority: result.priority,
          file: result.Track.mediaFile,
          name: result.Track.name_displayed,
          artists


        };
        mychart.push(obj);
      });
      return res.status(200).json({
        "chart": mychart, featured_slider, new_releases, featured_artist,
        featured_track, featured_album
      });

    });



  });



  app.get('/t/artist/tracks/:id', async (req, res) => {
    const { id } = req.params;


    if (id) {
      try {
        const artisttracks = await db.artist_track.findAll({

          where: {
            ArtistId: id
          },
          include: [{ model: db.track }
          ]

        }).
          then(function (result) {


            res.send(200).json({ result });

          });


      } catch (err) {
        console.log(err);
        return res.status(500).json({ err, msg: 'Internal server error' });
      }

    }
  });



  app.get('/t/track/:id?/:uid?', async (req, res) => {
    const { id, uid } = req.params;


    if (id || uid) {
      try {
        const track = await db.track.findOne({
          where: {
            $or: [{ uid: uid }, { id: id }]
          },
          include: [{
            model: db.primaryartist,
            include: [db.artist]
          }, {
            model: db.featuredartist,
            include: [db.artist]
          }, {
            model: db.remixers,
            include: [db.artist]
          }, {
            model: db.producers,
            include: [db.artist]
          }, {
            model: db.writers,
            include: [db.artist]
          },
          {
            model: db.album_track,
            include: [{ model: db.album }]
          }

          ],


        }).then(function (result) {
          // db.album_track.findOne({where {"id": }})
          // obj = JSON.parse(result);
          var album = result.albumtracks;
          var p_artist = result.primaryartists;
          var f_artist = result.featuredartists;
          var r_mixers = result.remixer_artists;
          var p_producers = result.producer_artists;
          var w_writers = result.writers_artists;
          var primaryartist = [];
          var featuredartists = [];
          var remixers = [];
          var producers = [];
          var remixers = [];
          var writers = [];
          p_artist.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image,
              artist_avatar220: resp.Artist.image220,
              artist_avatar150: resp.Artist.image150,
              artist_avatar140: resp.Artist.image140,
              artist_avatar110: resp.Artist.image110,
              artist_avatar70: resp.Artist.image70,
              artist_avatar50: resp.Artist.image50,
              artist_avatar38: resp.Artist.image38

            };
            primaryartist.push(obj);
          });
          f_artist.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image,
              artist_avatar220: resp.Artist.image220,
              artist_avatar150: resp.Artist.image150,
              artist_avatar140: resp.Artist.image140,
              artist_avatar110: resp.Artist.image110,
              artist_avatar70: resp.Artist.image70,
              artist_avatar50: resp.Artist.image50,
              artist_avatar38: resp.Artist.image38

            };
            featuredartists.push(obj);
          });
          r_mixers.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image,
              artist_avatar220: resp.Artist.image220,
              artist_avatar150: resp.Artist.image150,
              artist_avatar140: resp.Artist.image140,
              artist_avatar110: resp.Artist.image110,
              artist_avatar70: resp.Artist.image70,
              artist_avatar50: resp.Artist.image50,
              artist_avatar38: resp.Artist.image38

            };
            remixers.push(obj);
          });
          p_producers.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image,
              artist_avatar220: resp.Artist.image220,
              artist_avatar150: resp.Artist.image150,
              artist_avatar140: resp.Artist.image140,
              artist_avatar110: resp.Artist.image110,
              artist_avatar70: resp.Artist.image70,
              artist_avatar50: resp.Artist.image50,
              artist_avatar38: resp.Artist.image38
            };
            producers.push(obj);
          });
          w_writers.forEach(function (resp) {
            var obj = {
              id: resp.Artist.id,
              name: resp.Artist.name,
              artist_avatar: resp.Artist.image,
              artist_avatar220: resp.Artist.image220,
              artist_avatar150: resp.Artist.image150,
              artist_avatar140: resp.Artist.image140,
              artist_avatar110: resp.Artist.image110,
              artist_avatar70: resp.Artist.image70,
              artist_avatar50: resp.Artist.image50,
              artist_avatar38: resp.Artist.image38

            };
            writers.push(obj);
          });



          return res.status(200).json(200, {
            "trackID": result.id,
            "uid": result.uid,
            "track_name": result.name_displayed,
            "file": result.mediaFile,
            "line": result.line,
            "album_id": album[0].Album.id,
            "album_name": album[0].Album.name_displayed,
            "artwork": result.artwork,
            "artwork220": result.artwork220,
            "artwork150": result.artwork150,
            "artwork140": result.artwork140,
            "artwork110": result.artwork110,
            "artwork70": result.artwork70,
            "artwork50": result.artwork50,
            "artwork38": result.artwork38,
            "duration": result.duration,
            "primary_artists": primaryartist,
            "featured_artists": featuredartists,
            "remixers": remixers,
            "producers": producers,
            "writers": writers


          });

        });

        if (!track) {
          return res.status(400).json({ msg: 'Track not found' });
        }



      } catch (err) {
        console.log(err);
        return res.status(500).json({ err, msg: 'Internal server error' });
      }
    } else {

      return res.status(400).json({ msg: 'Please provide a value.' });
    }
  });

  app.get('/t/track/chart/:trackID/:priority', async (req, res) => {
    const { priority, trackID } = req.params;
    try {
      const chart = await db.top15chart.find(
        { where: { trackID: trackID } }
      )
        .then(function (obj) {
          if (obj) {
            db.top15chart.update({
              priority: priority,
              trackID: trackID,
              where: { trackID: trackID }
            });
          }
          else {
            db.top15chart.create({
              priority: priority,
              trackID: trackID,
            });
          }

        }).then(function (result) {
          return res.status(200).json({ result });
        });

    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    };

  });
  // fill routes for express appliction
  app.use('/public', mappedOpenRoutes);
  app.use('/private', mappedAuthRoutes);

  server.listen(config.port, () => {
    if (environment !== 'production' &&
      environment !== 'development' &&
      environment !== 'testing'
    ) {
      console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
      process.exit(1);
    }
    return DB;
  });